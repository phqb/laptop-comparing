create table CpuBenchmark
(
    Id           int identity
        constraint CpuBenchmark_pk
            primary key nonclustered,
    Name         nvarchar(1024) not null,
    PassmarkMark int            not null
)
go

create table CrawledProduct
(
    Id        int identity
        constraint CrawledProduct_pk
            primary key nonclustered,
    Url       varchar(1024) not null,
    Data      xml           not null,
    CrawledAt bigint        not null
)
go

create table Product
(
    Id                               int identity
        constraint Product_pk
            primary key nonclustered,
    Url                              nvarchar(1024) not null,
    ConditionIsNew                   bit            not null,
    ConditionNewPercent              int,
    Brand                            nvarchar(1024) not null,
    Model                            nvarchar(1024) not null,
    Price                            decimal(18),
    Status                           nvarchar(1024),
    JoinedAvailableAt                nvarchar(1024) not null,
    CpuTechnology                    nvarchar(1024),
    CpuType                          nvarchar(1024) not null,
    CpuSpeed                         nvarchar(1024),
    CpuCache                         nvarchar(1024),
    RamInGB                          int            not null,
    RamType                          nvarchar(1024) not null,
    RamBus                           nvarchar(1024),
    StorageType                      nvarchar(1024) not null,
    StorageAmountInGB                int            not null,
    ScreenSizeInInch                 decimal(18),
    ScreenResolutionType             nvarchar(1024) not null,
    ScreenResolutionWidth            int,
    ScreenResolutionHeight           int,
    ScreenTechnology                 nvarchar(1024),
    TouchScreen                      bit,
    GraphicCardName                  nvarchar(1024) not null,
    GraphicCardMemoryInGB            int,
    GraphicCardIntegratedOrDedicated bit            not null,
    BatteryInformation               nvarchar(1024),
    BatteryLifeInHourMin             decimal(18),
    BatteryLifeInHourMax             decimal(18),
    AudioTechnology                  nvarchar(1024),
    WebcamResolution                 nvarchar(1024),
    WeightInKg                       decimal(18),
    LastCrawledAt                    bigint         not null,
    BrandModel                       as [Brand] + ' ' + [Model]
)
go

create unique index Product_Url_uindex
    on Product (Url)
go


