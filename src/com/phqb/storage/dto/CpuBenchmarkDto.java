package com.phqb.storage.dto;

import java.io.Serializable;

public class CpuBenchmarkDto implements Serializable {

    private String name;

    private Integer passmarkMark;

    public CpuBenchmarkDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPassmarkMark() {
        return passmarkMark;
    }

    public void setPassmarkMark(Integer passmarkMark) {
        this.passmarkMark = passmarkMark;
    }
}
