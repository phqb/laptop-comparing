package com.phqb.storage.dto;

import java.io.Serializable;

public class ProductDto implements Serializable {

    protected Integer id;
    protected String url;
    protected Boolean conditionIsNew;
    protected Double conditionNewPercent;
    protected String brand;
    protected String model;
    protected Double price;
    protected String status;
    protected String joinedAvailableAt;
    protected String cpuTechnology;
    protected String cpuType;
    protected String cpuSpeed;
    protected String cpuCache;
    protected Integer ramInGB;
    protected String ramType;
    protected String ramBus;
    protected String storageType;
    protected Integer storageAmountInGB;
    protected Double screenSizeInInch;
    protected String screenResolutionType;
    protected Integer screenResolutionWidth;
    protected Integer screenResolutionHeight;
    protected String screenTechnology;
    protected Boolean touchScreen;
    protected String graphicCardName;
    protected Double graphicCardMemoryInGB;
    protected Boolean graphicCardIntegratedOrDedicated;
    protected String batteryInformation;
    protected Double batteryLifeInHourMin;
    protected Double batteryLifeInHourMax;
    protected String audioTechnology;
    protected String webcamResolution;
    protected Double weightInKg;
    protected Integer cpuPassmarkMark;
    protected Long lastCrawledAt;

    public ProductDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getConditionIsNew() {
        return conditionIsNew;
    }

    public void setConditionIsNew(Boolean conditionIsNew) {
        this.conditionIsNew = conditionIsNew;
    }

    public Double getConditionNewPercent() {
        return conditionNewPercent;
    }

    public void setConditionNewPercent(Double conditionNewPercent) {
        this.conditionNewPercent = conditionNewPercent;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJoinedAvailableAt() {
        return joinedAvailableAt;
    }

    public void setJoinedAvailableAt(String joinedAvailableAt) {
        this.joinedAvailableAt = joinedAvailableAt;
    }

    public String getCpuTechnology() {
        return cpuTechnology;
    }

    public void setCpuTechnology(String cpuTechnology) {
        this.cpuTechnology = cpuTechnology;
    }

    public String getCpuType() {
        return cpuType;
    }

    public void setCpuType(String cpuType) {
        this.cpuType = cpuType;
    }

    public String getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuSpeed(String cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public String getCpuCache() {
        return cpuCache;
    }

    public void setCpuCache(String cpuCache) {
        this.cpuCache = cpuCache;
    }

    public Integer getRamInGB() {
        return ramInGB;
    }

    public void setRamInGB(Integer ramInGB) {
        this.ramInGB = ramInGB;
    }

    public String getRamType() {
        return ramType;
    }

    public void setRamType(String ramType) {
        this.ramType = ramType;
    }

    public String getRamBus() {
        return ramBus;
    }

    public void setRamBus(String ramBus) {
        this.ramBus = ramBus;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public Integer getStorageAmountInGB() {
        return storageAmountInGB;
    }

    public void setStorageAmountInGB(Integer storageAmountInGB) {
        this.storageAmountInGB = storageAmountInGB;
    }

    public Double getScreenSizeInInch() {
        return screenSizeInInch;
    }

    public void setScreenSizeInInch(Double screenSizeInInch) {
        this.screenSizeInInch = screenSizeInInch;
    }

    public String getScreenResolutionType() {
        return screenResolutionType;
    }

    public void setScreenResolutionType(String screenResolutionType) {
        this.screenResolutionType = screenResolutionType;
    }

    public Integer getScreenResolutionWidth() {
        return screenResolutionWidth;
    }

    public void setScreenResolutionWidth(Integer screenResolutionWidth) {
        this.screenResolutionWidth = screenResolutionWidth;
    }

    public Integer getScreenResolutionHeight() {
        return screenResolutionHeight;
    }

    public void setScreenResolutionHeight(Integer screenResolutionHeight) {
        this.screenResolutionHeight = screenResolutionHeight;
    }

    public String getScreenTechnology() {
        return screenTechnology;
    }

    public void setScreenTechnology(String screenTechnology) {
        this.screenTechnology = screenTechnology;
    }

    public Boolean getTouchScreen() {
        return touchScreen;
    }

    public void setTouchScreen(Boolean touchScreen) {
        this.touchScreen = touchScreen;
    }

    public String getGraphicCardName() {
        return graphicCardName;
    }

    public void setGraphicCardName(String graphicCardName) {
        this.graphicCardName = graphicCardName;
    }

    public Double getGraphicCardMemoryInGB() {
        return graphicCardMemoryInGB;
    }

    public void setGraphicCardMemoryInGB(Double graphicCardMemoryInGB) {
        this.graphicCardMemoryInGB = graphicCardMemoryInGB;
    }

    public Boolean getGraphicCardIntegratedOrDedicated() {
        return graphicCardIntegratedOrDedicated;
    }

    public void setGraphicCardIntegratedOrDedicated(Boolean graphicCardIntegratedOrDedicated) {
        this.graphicCardIntegratedOrDedicated = graphicCardIntegratedOrDedicated;
    }

    public String getBatteryInformation() {
        return batteryInformation;
    }

    public void setBatteryInformation(String batteryInformation) {
        this.batteryInformation = batteryInformation;
    }

    public Double getBatteryLifeInHourMin() {
        return batteryLifeInHourMin;
    }

    public void setBatteryLifeInHourMin(Double batteryLifeInHourMin) {
        this.batteryLifeInHourMin = batteryLifeInHourMin;
    }

    public Double getBatteryLifeInHourMax() {
        return batteryLifeInHourMax;
    }

    public void setBatteryLifeInHourMax(Double batteryLifeInHourMax) {
        this.batteryLifeInHourMax = batteryLifeInHourMax;
    }

    public String getAudioTechnology() {
        return audioTechnology;
    }

    public void setAudioTechnology(String audioTechnology) {
        this.audioTechnology = audioTechnology;
    }

    public String getWebcamResolution() {
        return webcamResolution;
    }

    public void setWebcamResolution(String webcamResolution) {
        this.webcamResolution = webcamResolution;
    }

    public Double getWeightInKg() {
        return weightInKg;
    }

    public void setWeightInKg(Double weightInKg) {
        this.weightInKg = weightInKg;
    }

    public Integer getCpuPassmarkMark() {
        return cpuPassmarkMark;
    }

    public void setCpuPassmarkMark(Integer cpuPassmarkMark) {
        this.cpuPassmarkMark = cpuPassmarkMark;
    }

    public Long getLastCrawledAt() {
        return lastCrawledAt;
    }

    public void setLastCrawledAt(Long lastCrawledAt) {
        this.lastCrawledAt = lastCrawledAt;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", conditionIsNew=" + conditionIsNew +
                ", conditionNewPercent=" + conditionNewPercent +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", status='" + status + '\'' +
                ", joinedAvailableAt='" + joinedAvailableAt + '\'' +
                ", cpuTechnology='" + cpuTechnology + '\'' +
                ", cpuType='" + cpuType + '\'' +
                ", cpuSpeed='" + cpuSpeed + '\'' +
                ", cpuCache='" + cpuCache + '\'' +
                ", ramInGB=" + ramInGB +
                ", ramType='" + ramType + '\'' +
                ", ramBus='" + ramBus + '\'' +
                ", storageType='" + storageType + '\'' +
                ", storageAmountInGB=" + storageAmountInGB +
                ", screenSizeInInch=" + screenSizeInInch +
                ", screenResolutionType='" + screenResolutionType + '\'' +
                ", screenResolutionWidth=" + screenResolutionWidth +
                ", screenResolutionHeight=" + screenResolutionHeight +
                ", screenTechnology='" + screenTechnology + '\'' +
                ", touchScreen=" + touchScreen +
                ", graphicCardName='" + graphicCardName + '\'' +
                ", graphicCardMemoryInGB=" + graphicCardMemoryInGB +
                ", graphicCardIntegratedOrDedicated=" + graphicCardIntegratedOrDedicated +
                ", batteryInformation='" + batteryInformation + '\'' +
                ", batteryLifeInHourMin=" + batteryLifeInHourMin +
                ", batteryLifeInHourMax=" + batteryLifeInHourMax +
                ", audioTechnology='" + audioTechnology + '\'' +
                ", webcamResolution='" + webcamResolution + '\'' +
                ", weightInKg=" + weightInKg +
                ", cpuPassmarkMark=" + cpuPassmarkMark +
                ", lastCrawledAt=" + lastCrawledAt +
                '}';
    }
}
