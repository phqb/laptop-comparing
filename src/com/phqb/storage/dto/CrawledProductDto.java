package com.phqb.storage.dto;

import java.io.Serializable;

public class CrawledProductDto implements Serializable {

    protected String id;
    protected String url;
    protected String data;
    protected Long crawledAt;

    public CrawledProductDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getCrawledAt() {
        return crawledAt;
    }

    public void setCrawledAt(Long crawledAt) {
        this.crawledAt = crawledAt;
    }
}
