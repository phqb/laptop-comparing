package com.phqb.storage;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Utils {

    private static final String DATASOURCE_REF = "laptop_comparing";

    private static Boolean __AM_SERVER;

    private static boolean _amServer() {
        if (__AM_SERVER != null) {
            return __AM_SERVER;
        }

        __AM_SERVER = false;

        StackTraceElement[] elements = new Throwable().getStackTrace();

        for (StackTraceElement element : elements) {
            if (element.getClassName().equals("org.apache.catalina.core.StandardEngineValve")) {
                __AM_SERVER = true;
                break;
            }
        }

        return __AM_SERVER;
    }

    public static Connection getConnection() throws NamingException, SQLException {
        if (!_amServer()) {
            return DriverManager.getConnection("jdbc:sqlserver://localhost:1433;database=laptop_comparing", "SA", "D@rkerTh@11Bl@ck");
        } else {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource) envContext.lookup(DATASOURCE_REF);
            return ds.getConnection();
        }
    }
}
