package com.phqb.storage.dao;

import com.phqb.storage.Utils;
import com.phqb.storage.dto.CrawledProductDto;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CrawledProductDao {

    public boolean insert(CrawledProductDto crawledProductDto) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into CrawledProduct(Url, Data, CrawledAt) values (?, ?, ?)")) {
                int index = 0;
                preparedStatement.setString(++index, crawledProductDto.getUrl());
                preparedStatement.setString(++index, crawledProductDto.getData()
                        .replaceFirst("(<\\?\\s*xml.+?encoding=)\"(?:utf|UTF)-8\"", "$1\"UTF-16\""));
                preparedStatement.setLong(++index, crawledProductDto.getCrawledAt());
                return preparedStatement.execute();
            }
        }
    }
}
