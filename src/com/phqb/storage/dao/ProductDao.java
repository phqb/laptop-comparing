package com.phqb.storage.dao;

import com.phqb.storage.Utils;
import com.phqb.storage.dto.ProductDto;

import javax.naming.NamingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ProductDao {

    private static final Logger LOGGER = Logger.getLogger(ProductDao.class.getName());

    private static final Map<String, Integer> FIELD_TYPES = new HashMap<>();

    static {
        FIELD_TYPES.put("Id", Types.INTEGER);
        FIELD_TYPES.put("Url", Types.NVARCHAR);
        FIELD_TYPES.put("ConditionIsNew", Types.BIT);
        FIELD_TYPES.put("ConditionNewPercent", Types.DECIMAL);
        FIELD_TYPES.put("Brand", Types.NVARCHAR);
        FIELD_TYPES.put("Model", Types.NVARCHAR);
        FIELD_TYPES.put("Price", Types.DECIMAL);
        FIELD_TYPES.put("Status", Types.NVARCHAR);
        FIELD_TYPES.put("JoinedAvailableAt", Types.NVARCHAR);
        FIELD_TYPES.put("CpuTechnology", Types.NVARCHAR);
        FIELD_TYPES.put("CpuType", Types.NVARCHAR);
        FIELD_TYPES.put("CpuSpeed", Types.NVARCHAR);
        FIELD_TYPES.put("CpuCache", Types.NVARCHAR);
        FIELD_TYPES.put("RamInGB", Types.INTEGER);
        FIELD_TYPES.put("RamType", Types.NVARCHAR);
        FIELD_TYPES.put("RamBus", Types.NVARCHAR);
        FIELD_TYPES.put("StorageType", Types.NVARCHAR);
        FIELD_TYPES.put("StorageAmountInGB", Types.INTEGER);
        FIELD_TYPES.put("ScreenSizeInInch", Types.DECIMAL);
        FIELD_TYPES.put("ScreenResolutionType", Types.NVARCHAR);
        FIELD_TYPES.put("ScreenResolutionWidth", Types.INTEGER);
        FIELD_TYPES.put("ScreenResolutionHeight", Types.INTEGER);
        FIELD_TYPES.put("ScreenTechnology", Types.NVARCHAR);
        FIELD_TYPES.put("TouchScreen", Types.BIT);
        FIELD_TYPES.put("GraphicCardName", Types.NVARCHAR);
        FIELD_TYPES.put("GraphicCardMemoryInGB", Types.DECIMAL);
        FIELD_TYPES.put("GraphicCardIntegratedOrDedicated", Types.BIT);
        FIELD_TYPES.put("BatteryInformation", Types.NVARCHAR);
        FIELD_TYPES.put("BatteryLifeInHourMin", Types.DECIMAL);
        FIELD_TYPES.put("BatteryLifeInHourMax", Types.DECIMAL);
        FIELD_TYPES.put("AudioTechnology", Types.NVARCHAR);
        FIELD_TYPES.put("WebcamResolution", Types.NVARCHAR);
        FIELD_TYPES.put("WeightInKg", Types.DECIMAL);
        FIELD_TYPES.put("LastCrawledAt", Types.BIGINT);
        FIELD_TYPES.put("BrandModel", Types.NVARCHAR);
        FIELD_TYPES.put("CpuPassmarkMark", Types.INTEGER);
    }

    private void setProductFieldFromResultSet(ResultSet resultSet, ProductDto productDto, String field) throws SQLException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        switch (FIELD_TYPES.get(field)) {
            case Types.INTEGER: {
                Method method = ProductDto.class.getMethod("set" + field, Integer.class);
                Integer value = resultSet.getInt(field);
                if (resultSet.wasNull()) {
                    value = null;
                }
                method.invoke(productDto, value);

                break;
            }
            case Types.BIGINT: {
                Method method = ProductDto.class.getMethod("set" + field, Long.class);
                Long value = resultSet.getLong(field);
                if (resultSet.wasNull()) {
                    value = null;
                }
                method.invoke(productDto, value);

                break;
            }
            case Types.BIT: {
                Method method = ProductDto.class.getMethod("set" + field, Boolean.class);
                Boolean value = resultSet.getBoolean(field);
                if (resultSet.wasNull()) {
                    value = null;
                }
                method.invoke(productDto, value);

                break;
            }
            case Types.DECIMAL: {
                Method method = ProductDto.class.getMethod("set" + field, Double.class);
                Double value = resultSet.getDouble(field);
                if (resultSet.wasNull()) {
                    value = null;
                }
                method.invoke(productDto, value);

                break;
            }
            case Types.NVARCHAR: {
                Method method = ProductDto.class.getMethod("set" + field, String.class);
                String value = resultSet.getNString(field);
                if (resultSet.wasNull()) {
                    value = null;
                }
                method.invoke(productDto, value);

                break;
            }
        }
    }

    public List<ProductDto> getComparingProducts(List<String> comparingFields, List<Integer> ids) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            List<String> idsAsString = ids.stream().map(Object::toString).collect(Collectors.toList());
            String joinedIds = String.join(", ", idsAsString);

            Set<String> fields = new HashSet<>(comparingFields);
            fields.addAll(FIELD_TYPES.keySet());
            String joinedFields = String.join(", ", fields);

            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "select " + joinedFields + " from Product where Id in (" + joinedIds + ")"
            )) {
                List<ProductDto> productDtos = new LinkedList<>();

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        ProductDto productDto = new ProductDto();

                        for (String field : fields) {
                            try {
                                setProductFieldFromResultSet(resultSet, productDto, field);
                            } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException ignored) {
                            }
                        }

                        productDtos.add(productDto);
                    }
                    return productDtos;
                }
            }
        }
    }

    public long count(String keyword) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "select count(Id) from Product where BrandModel like ?")) {
                preparedStatement.setString(1, "%" + keyword + "%");
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getLong(1);
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    public List<String> getSearchKeywords() throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select distinct(BrandModel) from Product")) {

                List<String> keywords = new LinkedList<>();

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        keywords.add(resultSet.getNString(1));
                    }
                }

                return keywords;
            }
        }
    }

    public List<ProductDto> searchByLastCrawled(String keyword, int offset, int limit) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "select Id," +
                            "Url," +
                            "ConditionIsNew," +
                            "ConditionNewPercent," +
                            "Brand," +
                            "Model," +
                            "Price," +
                            "Status," +
                            "JoinedAvailableAt," +
                            "CpuTechnology," +
                            "CpuType," +
                            "CpuSpeed," +
                            "CpuCache," +
                            "RamInGB," +
                            "RamType," +
                            "RamBus," +
                            "StorageType," +
                            "StorageAmountInGB," +
                            "ScreenSizeInInch," +
                            "ScreenResolutionType," +
                            "ScreenResolutionWidth," +
                            "ScreenResolutionHeight," +
                            "ScreenTechnology," +
                            "TouchScreen," +
                            "GraphicCardName," +
                            "GraphicCardMemoryInGB," +
                            "GraphicCardIntegratedOrDedicated," +
                            "BatteryInformation," +
                            "BatteryLifeInHourMin," +
                            "BatteryLifeInHourMax," +
                            "AudioTechnology," +
                            "WebcamResolution," +
                            "WeightInKg, " +
                            "CpuPassmarkMark " +
                            "from Product " +
                            "where (BrandModel) like ? " +
                            "order by LastCrawledAt desc " +
                            "offset (?) rows " +
                            "fetch next (?) rows only")) {
                preparedStatement.setString(1, "%" + keyword + "%");
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, limit);

                List<ProductDto> products = new LinkedList<>();

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        ProductDto productDto = new ProductDto();
                        productDto.setId(resultSet.getInt(1));
                        productDto.setUrl(resultSet.getNString("Url"));
                        boolean conditionIsNew = resultSet.getBoolean("ConditionIsNew");
                        if (resultSet.wasNull()) {
                            productDto.setConditionIsNew(null);
                        } else {
                            productDto.setConditionIsNew(conditionIsNew);
                        }
                        double conditionNewPercent = resultSet.getDouble("ConditionNewPercent");
                        if (resultSet.wasNull()) {
                            productDto.setConditionNewPercent(null);
                        } else {
                            productDto.setConditionNewPercent(conditionNewPercent);
                        }
                        productDto.setBrand(resultSet.getNString("Brand"));
                        productDto.setModel(resultSet.getNString("Model"));
                        double price = resultSet.getDouble("Price");
                        if (resultSet.wasNull()) {
                            productDto.setPrice(null);
                        } else {
                            productDto.setPrice(price);
                        }
                        productDto.setStatus(resultSet.getNString("Status"));
                        productDto.setJoinedAvailableAt(resultSet.getNString("JoinedAvailableAt"));
                        productDto.setCpuTechnology(resultSet.getNString("CpuTechnology"));
                        productDto.setCpuType(resultSet.getNString("CpuType"));
                        productDto.setCpuSpeed(resultSet.getNString("CpuSpeed"));
                        productDto.setCpuCache(resultSet.getNString("CpuCache"));
                        int ramInGB = resultSet.getInt("RamInGB");
                        if (resultSet.wasNull()) {
                            productDto.setRamInGB(null);
                        } else {
                            productDto.setRamInGB(ramInGB);
                        }
                        productDto.setRamType(resultSet.getNString("RamType"));
                        productDto.setRamBus(resultSet.getNString("RamBus"));
                        productDto.setStorageType(resultSet.getNString("StorageType"));
                        int storageAmountInGB = resultSet.getInt("StorageAmountInGB");
                        if (resultSet.wasNull()) {
                            productDto.setStorageAmountInGB(null);
                        } else {
                            productDto.setStorageAmountInGB(storageAmountInGB);
                        }
                        double screenSizeInInch = resultSet.getDouble("ScreenSizeInInch");
                        if (resultSet.wasNull()) {
                            productDto.setScreenSizeInInch(null);
                        } else {
                            productDto.setScreenSizeInInch(screenSizeInInch);
                        }
                        productDto.setScreenResolutionType(resultSet.getNString("ScreenResolutionType"));
                        int screenResolutionWidth = resultSet.getInt("ScreenResolutionWidth");
                        if (resultSet.wasNull()) {
                            productDto.setScreenResolutionWidth(null);
                        } else {
                            productDto.setScreenResolutionWidth(screenResolutionWidth);
                        }
                        int screenResolutionHeight = resultSet.getInt("ScreenResolutionHeight");
                        if (resultSet.wasNull()) {
                            productDto.setScreenResolutionHeight(null);
                        } else {
                            productDto.setScreenResolutionHeight(screenResolutionHeight);
                        }
                        productDto.setScreenTechnology(resultSet.getNString("ScreenTechnology"));
                        boolean touchScreen = resultSet.getBoolean("TouchScreen");
                        if (resultSet.wasNull()) {
                            productDto.setTouchScreen(null);
                        } else {
                            productDto.setTouchScreen(touchScreen);
                        }
                        productDto.setGraphicCardName(resultSet.getNString("GraphicCardName"));
                        double graphicCardMemoryInGB = resultSet.getDouble("GraphicCardMemoryInGB");
                        if (resultSet.wasNull()) {
                            productDto.setGraphicCardMemoryInGB(null);
                        } else {
                            productDto.setGraphicCardMemoryInGB(graphicCardMemoryInGB);
                        }
                        boolean graphicCardIntegratedOrDedicated = resultSet.getBoolean("GraphicCardIntegratedOrDedicated");
                        if (resultSet.wasNull()) {
                            productDto.setGraphicCardIntegratedOrDedicated(null);
                        } else {
                            productDto.setGraphicCardIntegratedOrDedicated(graphicCardIntegratedOrDedicated);
                        }
                        productDto.setBatteryInformation(resultSet.getNString("BatteryInformation"));
                        double batteryLifeInHourMin = resultSet.getDouble("BatteryLifeInHourMin");
                        if (resultSet.wasNull()) {
                            productDto.setBatteryLifeInHourMin(null);
                        } else {
                            productDto.setBatteryLifeInHourMin(batteryLifeInHourMin);
                        }
                        double batteryLifeInHourMax = resultSet.getDouble("BatteryLifeInHourMax");
                        if (resultSet.wasNull()) {
                            productDto.setBatteryLifeInHourMax(null);
                        } else {
                            productDto.setBatteryLifeInHourMax(batteryLifeInHourMax);
                        }
                        productDto.setAudioTechnology(resultSet.getNString("AudioTechnology"));
                        productDto.setWebcamResolution(resultSet.getNString("WebcamResolution"));
                        double weightInKg = resultSet.getDouble("WeightInKg");
                        if (resultSet.wasNull()) {
                            productDto.setWeightInKg(null);
                        } else {
                            productDto.setWeightInKg(weightInKg);
                        }
                        int passmarkMark = resultSet.getInt("CpuPassmarkMark");
                        if (resultSet.wasNull()) {
                            productDto.setCpuPassmarkMark(null);
                        } else {
                            productDto.setCpuPassmarkMark(passmarkMark);
                        }

                        products.add(productDto);
                    }
                }

                return products;
            }
        }
    }

    public boolean upsert(ProductDto productDto) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "if not exists (select Id from Product where Url = ?) " +
                            "insert into Product (Url," +
                            "ConditionIsNew," +
                            "ConditionNewPercent," +
                            "Brand," +
                            "Model," +
                            "Price," +
                            "Status," +
                            "JoinedAvailableAt," +
                            "CpuTechnology," +
                            "CpuType," +
                            "CpuSpeed," +
                            "CpuCache," +
                            "RamInGB," +
                            "RamType," +
                            "RamBus," +
                            "StorageType," +
                            "StorageAmountInGB," +
                            "ScreenSizeInInch," +
                            "ScreenResolutionType," +
                            "ScreenResolutionWidth," +
                            "ScreenResolutionHeight," +
                            "ScreenTechnology," +
                            "TouchScreen," +
                            "GraphicCardName," +
                            "GraphicCardMemoryInGB," +
                            "GraphicCardIntegratedOrDedicated," +
                            "BatteryInformation," +
                            "BatteryLifeInHourMin," +
                            "BatteryLifeInHourMax," +
                            "AudioTechnology," +
                            "WebcamResolution," +
                            "WeightInKg," +
                            "LastCrawledAt" +
                            ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " +
                            "else update Product set " +
                            "ConditionIsNew = ?, " +
                            "ConditionNewPercent = ?, " +
                            "Brand = ?, " +
                            "Model = ?, " +
                            "Price = ?, " +
                            "Status = ?, " +
                            "JoinedAvailableAt = ?, " +
                            "CpuTechnology = ?, " +
                            "CpuType = ?, " +
                            "CpuSpeed = ?, " +
                            "CpuCache = ?, " +
                            "RamInGB = ?, " +
                            "RamType = ?, " +
                            "RamBus = ?, " +
                            "StorageType = ?, " +
                            "StorageAmountInGB = ?, " +
                            "ScreenSizeInInch = ?, " +
                            "ScreenResolutionType = ?, " +
                            "ScreenResolutionWidth = ?, " +
                            "ScreenResolutionHeight = ?, " +
                            "ScreenTechnology = ?, " +
                            "TouchScreen = ?, " +
                            "GraphicCardName = ?, " +
                            "GraphicCardMemoryInGB = ?, " +
                            "GraphicCardIntegratedOrDedicated = ?, " +
                            "BatteryInformation = ?, " +
                            "BatteryLifeInHourMin = ?, " +
                            "BatteryLifeInHourMax = ?, " +
                            "AudioTechnology = ?, " +
                            "WebcamResolution = ?, " +
                            "WeightInKg = ?, " +
                            "LastCrawledAt = ? " +
                            "where Url = ?"

            )) {
                int index = 0;
                preparedStatement.setObject(++index, productDto.getUrl(), Types.NVARCHAR);
                preparedStatement.setObject(++index, productDto.getUrl(), Types.NVARCHAR);

                for (int i = 0; i < 2; i += 1) {
                    preparedStatement.setObject(++index, productDto.getConditionIsNew(), Types.BIT);
                    preparedStatement.setObject(++index, productDto.getConditionNewPercent(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getBrand(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getModel(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getPrice(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getStatus(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getJoinedAvailableAt(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getCpuTechnology(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getCpuType(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getCpuSpeed(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getCpuCache(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getRamInGB(), Types.INTEGER);
                    preparedStatement.setObject(++index, productDto.getRamType(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getRamBus(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getStorageType(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getStorageAmountInGB(), Types.INTEGER);
                    preparedStatement.setObject(++index, productDto.getScreenSizeInInch(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getScreenResolutionType(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getScreenResolutionWidth(), Types.INTEGER);
                    preparedStatement.setObject(++index, productDto.getScreenResolutionHeight(), Types.INTEGER);
                    preparedStatement.setObject(++index, productDto.getScreenTechnology(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getTouchScreen(), Types.BIT);
                    preparedStatement.setObject(++index, productDto.getGraphicCardName(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getGraphicCardMemoryInGB(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getGraphicCardIntegratedOrDedicated(), Types.BIT);
                    preparedStatement.setObject(++index, productDto.getBatteryInformation(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getBatteryLifeInHourMin(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getBatteryLifeInHourMax(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getAudioTechnology(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getWebcamResolution(), Types.NVARCHAR);
                    preparedStatement.setObject(++index, productDto.getWeightInKg(), Types.DECIMAL);
                    preparedStatement.setObject(++index, productDto.getLastCrawledAt(), Types.BIGINT);
                }

                preparedStatement.setObject(++index, productDto.getUrl(), Types.NVARCHAR);

                return preparedStatement.execute();
            }
        }
    }
}
