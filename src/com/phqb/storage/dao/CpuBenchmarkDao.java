package com.phqb.storage.dao;

import com.phqb.storage.Utils;
import com.phqb.storage.dto.CpuBenchmarkDto;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public class CpuBenchmarkDao {

    private static final Logger LOGGER = Logger.getLogger(CpuBenchmarkDao.class.getName());

    public boolean upsert(CpuBenchmarkDto cpuBenchmarkDto) throws SQLException, NamingException {
        try (Connection connection = Utils.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "if not exists(select Id from CpuBenchmark where Name = ?) " +
                            "insert into CpuBenchmark(Name, PassmarkMark) values(?, ?) " +
                            "else update CpuBenchmark set PassmarkMark = ? where Name = ?"
            )) {
                preparedStatement.setString(1, cpuBenchmarkDto.getName());
                preparedStatement.setString(2, cpuBenchmarkDto.getName());
                preparedStatement.setInt(3, cpuBenchmarkDto.getPassmarkMark());
                preparedStatement.setInt(4, cpuBenchmarkDto.getPassmarkMark());
                preparedStatement.setString(5, cpuBenchmarkDto.getName());
                return preparedStatement.execute();
            }
        }
    }
}
