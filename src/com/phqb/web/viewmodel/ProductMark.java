package com.phqb.web.viewmodel;

import java.io.Serializable;

public class ProductMark implements Serializable {

    private int productId;

    private Double mark;

    public ProductMark() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }
}
