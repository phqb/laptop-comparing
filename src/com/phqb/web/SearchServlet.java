package com.phqb.web;

import com.phqb.storage.dao.ProductDao;
import com.phqb.storage.dto.ProductDto;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SearchServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(SearchServlet.class.getName());

    private static final String SEARCH_PAGE = "/WEB-INF/index.jsp";

    private static final String PARAM_KEYWORD = "keyword";
    private static final String PARAM_OFFSET = "offset";
    private static final String PARAM_LIMIT = "limit";

    private static final int DEFAULT_OFFSET = 0;
    private static final int DEFAULT_LIMIT = 20;

    private static final ProductDao PRODUCT_DAO = new ProductDao();
    private static final String KEY_SEARCH_RESULT = "searchResult";
    private static final String KEY_NUM_OF_PAGES = "numberOfPages";
    private static final String KEY_CURRENT_PAGE = "currentPage";
    private static final String KEY_CURRENT_LIMIT = "currentLimit";
    private static final String KEY_CURRENT_OFFSET = "currentOffset";
    private static final String KEY_CURRENT_KEYWORD = "currentKeyword";
    private static final String KEY_PAGE_NUMBER_LIST = "pageNumberList";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String keyword;
        if (request.getParameter(PARAM_KEYWORD) != null) {
            keyword = request.getParameter(PARAM_KEYWORD).trim();
        } else {
            keyword = "";
        }

        int offset = DEFAULT_OFFSET;
        if (request.getParameter(PARAM_OFFSET) != null) {
            try {
                offset = Integer.parseInt(request.getParameter(PARAM_OFFSET));
            } catch (NumberFormatException ignored) {
            }
        }

        int limit = DEFAULT_LIMIT;
        if (request.getParameter(PARAM_LIMIT) != null) {
            try {
                limit = Integer.parseInt(request.getParameter(PARAM_LIMIT));
                if (limit <= 0) {
                    limit = DEFAULT_LIMIT;
                }
            } catch (NumberFormatException ignored) {
            }
        }

        try {
            List<ProductDto> productDtos = PRODUCT_DAO.searchByLastCrawled(keyword, offset, limit);

            long productsCount = PRODUCT_DAO.count(keyword);
            int numOfPages = (int) Math.round(Math.ceil((double) productsCount / limit));
            int currentPage = (int) (Math.round(Math.floor((double) offset / limit)) + 1);

            request.setAttribute(KEY_SEARCH_RESULT, productDtos);
            request.setAttribute(KEY_NUM_OF_PAGES, numOfPages);
            request.setAttribute(KEY_CURRENT_PAGE, currentPage);
            request.setAttribute(KEY_CURRENT_LIMIT, limit);
            request.setAttribute(KEY_CURRENT_OFFSET, offset);
            request.setAttribute(KEY_CURRENT_KEYWORD, keyword);

            List<Integer> pageNumberList = new LinkedList<>();
            pageNumberList.add(1);
            for (int p = currentPage - 3; p < currentPage; p += 1) {
                if (p > 1) {
                    pageNumberList.add(p);
                }
            }
            if (currentPage != 1 && currentPage != numOfPages) {
                pageNumberList.add(currentPage);
            }
            for (int p = currentPage + 1; p < currentPage + 4; p += 1) {
                if (p < numOfPages) {
                    pageNumberList.add(p);
                }
            }
            pageNumberList.add(numOfPages);

            request.setAttribute(KEY_PAGE_NUMBER_LIST, pageNumberList);

        } catch (SQLException | NamingException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        request.getRequestDispatcher(SEARCH_PAGE).forward(request, response);
    }
}
