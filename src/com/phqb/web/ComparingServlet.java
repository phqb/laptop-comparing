package com.phqb.web;

import com.phqb.storage.dao.ProductDao;
import com.phqb.storage.dto.ProductDto;
import com.phqb.utils.Utils;
import com.phqb.web.viewmodel.ProductMark;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComparingServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ComparingServlet.class.getName());

    private static final String INVALID_FIELDS_PAGE = "/WEB-INF/invalidFields.jsp";
    private static final String COMPARING_RESULT_PAGE = "/WEB-INF/comparingResult.jsp";

    public static final Set<String> COMPARABLE_FIELDS = new HashSet<>();
    public static final Set<String> LOWER_IS_BETTER = new HashSet<>();

    private static final String PARAM_COMPARING_PRODUCT_IDS = "comparingIds";
    private static final String PARAM_COMPARING_FIELDS = "comparingFields";

    static {
        COMPARABLE_FIELDS.add("ConditionNewPercent");
        COMPARABLE_FIELDS.add("Price");
        COMPARABLE_FIELDS.add("CpuPassmarkMark");
        COMPARABLE_FIELDS.add("RamInGB");
        COMPARABLE_FIELDS.add("StorageAmountInGB");
        COMPARABLE_FIELDS.add("ScreenSizeInInch");
        COMPARABLE_FIELDS.add("ScreenResolutionWidth");
        COMPARABLE_FIELDS.add("GraphicCardMemoryInGB");
        COMPARABLE_FIELDS.add("GraphicCardIntegratedOrDedicated");
        LOWER_IS_BETTER.add("Price");
        LOWER_IS_BETTER.add("GraphicCardIntegratedOrDedicated");
    }

    private static final ProductDao PRODUCT_DAO = new ProductDao();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] comparingProductIdsAsString = request.getParameterValues(PARAM_COMPARING_PRODUCT_IDS);

        if (comparingProductIdsAsString == null || comparingProductIdsAsString.length < 1) {
            request.setAttribute("errorMessage", "Vui lòng chọn ít nhất 2 sản phẩm.");
            request.getRequestDispatcher(INVALID_FIELDS_PAGE).forward(request, response);
            return;
        }

        List<Integer> comparingProductIds = new LinkedList<>();

        for (String comparingProductIdAsString : comparingProductIdsAsString) {
            try {
                comparingProductIds.add(Integer.parseInt(comparingProductIdAsString));
            } catch (NumberFormatException ignored) {
                request.setAttribute("errorMessage", "Sản phẩm với mã " +
                        comparingProductIdAsString + " không tồn tại.");
                request.getRequestDispatcher(INVALID_FIELDS_PAGE).forward(request, response);
                return;
            }
        }

        String[] comparingFields = request.getParameterValues(PARAM_COMPARING_FIELDS);

        if (comparingFields == null || comparingFields.length < 1) {
            request.setAttribute("errorMessage", "Vui lòng chọn ít nhất 1 tiêu chí.");
            request.getRequestDispatcher(INVALID_FIELDS_PAGE).forward(request, response);
            return;
        }

        for (String comparingField : comparingFields) {
            if (!COMPARABLE_FIELDS.contains(comparingField)) {
                request.setAttribute("errorMessage", "Không thể so sánh sản phẩm dựa trên tiêu chí " +
                        Utils.localizeProductCompareCriteria(comparingField) + ".");
                request.getRequestDispatcher(INVALID_FIELDS_PAGE).forward(request, response);
                return;
            }
        }

        try {
            List<ProductDto> comparingProducts =
                    PRODUCT_DAO.getComparingProducts(Arrays.asList(comparingFields), comparingProductIds);

            Map<String, Double> fieldsMaxValue = new HashMap<>();

            for (String field : comparingFields) {
                Method getter = ProductDto.class.getMethod("get" + field);

                Double maxValue = null;
                for (ProductDto productDto : comparingProducts) {
                    Object valueAsObject = getter.invoke(productDto);
                    Double value;
                    if (valueAsObject instanceof Double) {
                        value = (Double) valueAsObject;
                    } else if (valueAsObject instanceof Integer) {
                        value = ((Integer) valueAsObject).doubleValue();
                    } else {
                        value = null;
                    }

                    if (maxValue == null || (value != null && value > maxValue)) {
                        maxValue = value;
                    }
                }

                fieldsMaxValue.put(field, maxValue);
            }

            List<ProductMark> productMarks = new LinkedList<>();
            Map<Integer, ProductMark> productMarkMap = new HashMap<>();

            for (ProductDto productDto : comparingProducts) {
                int weight = comparingFields.length;
                double score = 0;

                for (String field : comparingFields) {
                    if (fieldsMaxValue.get(field) != null && fieldsMaxValue.get(field) > 0) {
                        Method getter = ProductDto.class.getMethod("get" + field);
                        Object valueAsObject = getter.invoke(productDto);
                        Double value;
                        if (valueAsObject instanceof Double) {
                            value = (Double) valueAsObject;
                        } else if (valueAsObject instanceof Integer) {
                            value = ((Integer) valueAsObject).doubleValue();
                        } else {
                            value = null;
                        }

                        if (value != null) {
                            double fieldScore = value / fieldsMaxValue.get(field);

                            if (LOWER_IS_BETTER.contains(field)) {
                                fieldScore = 1 - fieldScore;
                            }

                            score += fieldScore * weight;
                        }
                    }

                    weight -= 1;
                }

                ProductMark productMark = new ProductMark();
                productMark.setProductId(productDto.getId());
                productMark.setMark(score);

                productMarks.add(productMark);
                productMarkMap.put(productDto.getId(), productMark);
            }

            comparingProducts.sort((productA, productB) -> {
                double productAMark = productMarkMap.get(productA.getId()).getMark();
                double productBMark = productMarkMap.get(productB.getId()).getMark();
                if (productAMark == productBMark) {
                    return productA.getId().compareTo(productB.getId());
                } else if (productAMark > productBMark) {
                    return -1;
                } else {
                    return 1;
                }
            });

            productMarks.sort((markA, markB) -> {
                if (markA.getMark().equals(markB.getMark())) {
                    return Integer.compare(markA.getProductId(), markB.getProductId());
                } else if (markA.getMark() > markB.getMark()) {
                    return -1;
                } else {
                    return 1;
                }
            });

            request.setAttribute("comparingProducts", comparingProducts);
            request.setAttribute("productMarks", productMarks);
            request.setAttribute("comparingFields", comparingFields);

            request.getRequestDispatcher(COMPARING_RESULT_PAGE).forward(request, response);
        } catch (SQLException | NamingException | NoSuchMethodException |
                IllegalAccessException | InvocationTargetException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
