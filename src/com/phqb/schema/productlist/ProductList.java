package com.phqb.schema.productlist;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductList {

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "product", propOrder = { "url", "rawStatus" })
    public static class ProductListItem {

        @XmlElement
        private String url;
        @XmlElement
        private String rawStatus;

        public ProductListItem() {
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getRawStatus() {
            return rawStatus;
        }

        public void setRawStatus(String rawStatus) {
            this.rawStatus = rawStatus;
        }
    }

    @XmlElement(name = "product")
    private List<ProductListItem> products;

    public ProductList() {
    }

    public List<ProductListItem> getProducts() {
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }

    public void setProducts(List<ProductListItem> products) {
        this.products = products;
    }
}
