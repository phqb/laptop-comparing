package com.phqb.schema;

import com.phqb.schema.cpubenchmarks.Cpu;
import com.phqb.schema.product.Condition;
import com.phqb.schema.product.GraphicCard;
import com.phqb.schema.product.Locations;
import com.phqb.schema.product.Product;
import com.phqb.schema.product.ScreenResolution;
import com.phqb.storage.dto.CpuBenchmarkDto;
import com.phqb.storage.dto.ProductDto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class Utils {

    public static Product convertFromProductDto(ProductDto productDto) {
        Product product = new Product();

        product.setUrl(productDto.getUrl());
        Condition productCondition = new Condition();
        productCondition.setIsNew(productDto.getConditionIsNew());
        if (productDto.getConditionNewPercent() != null) {
            productCondition.setNewPercent(BigDecimal.valueOf(productDto.getConditionNewPercent()));
        } else {
            productCondition.setNewPercent(null);
        }
        product.setCondition(productCondition);
        product.setBrand(productDto.getBrand());
        product.setModel(productDto.getModel());
        if (productDto.getPrice() != null) {
            product.setPrice(BigDecimal.valueOf(productDto.getPrice()));
        } else {
            product.setPrice(null);
        }
        product.setStatus(productDto.getStatus());
        String[] splitedLocations = productDto.getJoinedAvailableAt().split(", ");
        Locations locations = new Locations();
        locations.setLocation(Arrays.asList(splitedLocations));
        product.setAvailableAt(locations);
        product.setCpuTechnology(productDto.getCpuTechnology());
        product.setCpuType(productDto.getCpuType());
        product.setCpuSpeed(productDto.getCpuSpeed());
        product.setCpuCache(productDto.getCpuCache());
        product.setRamInGB(BigInteger.valueOf(productDto.getRamInGB()));
        product.setRamType(productDto.getRamType());
        product.setRamBus(productDto.getRamBus());
        product.setStorageType(productDto.getStorageType());
        product.setStorageAmountInGB(BigInteger.valueOf(productDto.getStorageAmountInGB()));
        if (productDto.getScreenSizeInInch() != null) {
            product.setScreenSizeInInch(BigDecimal.valueOf(productDto.getScreenSizeInInch()));
        } else {
            product.setScreenSizeInInch(null);
        }
        ScreenResolution screenResolution = new ScreenResolution();
        screenResolution.setType(productDto.getScreenResolutionType());
        if (productDto.getScreenResolutionWidth() != null) {
            screenResolution.setWidth(BigInteger.valueOf(productDto.getScreenResolutionWidth()));
        } else {
            screenResolution.setWidth(null);
        }
        if (productDto.getScreenResolutionHeight() != null) {
            screenResolution.setHeight(BigInteger.valueOf(productDto.getScreenResolutionHeight()));
        } else {
            screenResolution.setHeight(null);
        }
        product.setScreenResolution(screenResolution);
        product.setScreenTechnology(productDto.getScreenTechnology());
        product.setTouchScreen(productDto.getTouchScreen());
        GraphicCard graphicCard = new GraphicCard();
        graphicCard.setName(productDto.getGraphicCardName());
        graphicCard.setIntegratedOrDedicated(productDto.getGraphicCardIntegratedOrDedicated());
        if (productDto.getGraphicCardMemoryInGB() != null) {
            graphicCard.setMemoryInGB(BigDecimal.valueOf(productDto.getGraphicCardMemoryInGB()));
        } else {
            graphicCard.setMemoryInGB(null);
        }
        product.setGraphicCard(graphicCard);
        Product.BatteryLifeInHour batteryLifeInHour = new Product.BatteryLifeInHour();
        if (productDto.getBatteryLifeInHourMin() != null) {
            batteryLifeInHour.setMin(BigDecimal.valueOf(productDto.getBatteryLifeInHourMin()));
        } else {
            batteryLifeInHour.setMin(null);
        }
        if (productDto.getBatteryLifeInHourMax() != null) {
            batteryLifeInHour.setMax(BigDecimal.valueOf(productDto.getBatteryLifeInHourMax()));
        } else {
            batteryLifeInHour.setMax(null);
        }
        product.setBatteryLifeInHour(batteryLifeInHour);
        product.setAudioTechnology(productDto.getAudioTechnology());
        product.setWebcamResolution(productDto.getWebcamResolution());
        if (productDto.getWeightInKg() != null) {
            product.setWeightInKg(BigDecimal.valueOf(productDto.getWeightInKg()));
        } else {
            product.setWeightInKg(null);
        }

        return product;
    }

    public static ProductDto convertToProductDto(Product product) {
        ProductDto productDto = new ProductDto();

        productDto.setUrl(product.getUrl());
        productDto.setConditionIsNew(product.getCondition().isIsNew());
        if (product.getCondition().getNewPercent() != null) {
            productDto.setConditionNewPercent(product.getCondition().getNewPercent().doubleValue());
        } else {
            productDto.setConditionNewPercent(null);
        }
        productDto.setBrand(product.getBrand());
        productDto.setModel(product.getModel());
        if (product.getPrice() != null) {
            productDto.setPrice(product.getPrice().doubleValue());
        } else {
            productDto.setPrice(null);
        }
        productDto.setStatus(product.getStatus());
        productDto.setJoinedAvailableAt(String.join(", ", product.getAvailableAt().getLocation()));
        productDto.setCpuTechnology(product.getCpuTechnology());
        productDto.setCpuType(product.getCpuType());
        productDto.setCpuSpeed(product.getCpuSpeed());
        productDto.setCpuCache(product.getCpuCache());
        productDto.setRamInGB(product.getRamInGB().intValue());
        productDto.setRamType(product.getRamType());
        productDto.setRamBus(product.getRamBus());
        productDto.setStorageType(product.getStorageType());
        productDto.setStorageAmountInGB(product.getStorageAmountInGB().intValue());
        if (product.getScreenSizeInInch() != null) {
            productDto.setScreenSizeInInch(product.getScreenSizeInInch().doubleValue());
        } else {
            productDto.setScreenSizeInInch(null);
        }
        productDto.setScreenResolutionType(product.getScreenResolution().getType());
        if (product.getScreenResolution().getWidth() != null) {
            productDto.setScreenResolutionWidth(product.getScreenResolution().getWidth().intValue());
        } else {
            productDto.setScreenResolutionWidth(null);
        }
        if (product.getScreenResolution().getHeight() != null) {
            productDto.setScreenResolutionHeight(product.getScreenResolution().getHeight().intValue());
        } else {
            productDto.setScreenResolutionHeight(null);
        }
        productDto.setScreenTechnology(product.getScreenTechnology());
        productDto.setTouchScreen(product.isTouchScreen());
        productDto.setGraphicCardName(product.getGraphicCard().getName());
        if (product.getGraphicCard().getMemoryInGB() != null) {
            productDto.setGraphicCardMemoryInGB(product.getGraphicCard().getMemoryInGB().doubleValue());
        } else {
            productDto.setGraphicCardMemoryInGB(null);
        }
        productDto.setGraphicCardIntegratedOrDedicated(product.getGraphicCard().isIntegratedOrDedicated());
        productDto.setBatteryInformation(product.getBatteryInformation());
        if (product.getBatteryLifeInHour().getMin() != null) {
            productDto.setBatteryLifeInHourMin(product.getBatteryLifeInHour().getMin().doubleValue());
        } else {
            productDto.setBatteryLifeInHourMin(null);
        }
        if (product.getBatteryLifeInHour().getMax() != null) {
            productDto.setBatteryLifeInHourMax(product.getBatteryLifeInHour().getMax().doubleValue());
        } else {
            productDto.setBatteryLifeInHourMax(null);
        }
        productDto.setAudioTechnology(product.getAudioTechnology());
        productDto.setWebcamResolution(product.getWebcamResolution());
        if (product.getWeightInKg() != null) {
            productDto.setWeightInKg(product.getWeightInKg().doubleValue());
        } else {
            productDto.setWeightInKg(null);
        }
        productDto.setLastCrawledAt(System.currentTimeMillis());

        return productDto;
    }

    public static CpuBenchmarkDto convertToCpuBenchmarkDto(Cpu cpuBenchmark) {
        CpuBenchmarkDto cpuBenchmarkDto = new CpuBenchmarkDto();
        cpuBenchmarkDto.setName(cpuBenchmark.getName());
        cpuBenchmarkDto.setPassmarkMark(cpuBenchmark.getPassmarkMark().intValue());
        return cpuBenchmarkDto;
    }
}
