package com.phqb.crawler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static String tryMatch(Pattern[] patterns, String string) {
        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(string);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }

        return "";
    }

    public static String extractCpuName(String productTitle) {
        final Pattern[] patterns = {
                Pattern.compile("((?:intel\\s+)?core\\s+i?\\d+(?:(?:\\s+|\\s*-\\s*)\\w+)?)", Pattern.CASE_INSENSITIVE),
                Pattern.compile("((?:amd\\s+)?ryzen\\s+\\d+(?:\\s+\\w+)?)", Pattern.CASE_INSENSITIVE)
        };

        return tryMatch(patterns, productTitle);
    }

    public static String extractProductBrand(String productTitle) {
        String[] tokens = productTitle.trim().split("\\s+");
        if (tokens.length > 0) {
            return tokens[0];
        } else {
            return "";
        }
    }

    public static String extractProductModel(String productTitle) {
        String[] tokens = productTitle.trim().split("\\s+");
        if (tokens.length > 1) {
            StringBuilder modelBuilder = new StringBuilder();
            for (int i = 1; i < tokens.length; i += 1) {
                String loweredToken = tokens[i].toLowerCase();
                if (loweredToken.startsWith("intel")
                        || loweredToken.startsWith("core")
                        || loweredToken.startsWith("amd")
                        || loweredToken.startsWith("ryzen")
                        || loweredToken.startsWith("-")
                        || loweredToken.startsWith("i3")
                        || loweredToken.startsWith("i5")
                        || loweredToken.startsWith("i7")
                        || loweredToken.startsWith("i9")
                        || loweredToken.startsWith("xeon")
                        || loweredToken.startsWith("(")
                        || loweredToken.startsWith("[")
                ) {
                    break;
                } else {
                    modelBuilder.append(tokens[i]).append(' ');
                }
            }
            return modelBuilder.toString().trim();
        } else {
            return "";
        }
    }

    public static String extractRamAmount(String info) {
        final Pattern[] patterns = {
                Pattern.compile("RAM\\s+(\\d+)"),
                Pattern.compile("(?:(?:LP)?DDR\\dL?.*?)?(?:RAM.*?)?(\\d+)\\s*GB?(?:RAM.*)?(?:(?:LP)?DDR\\dL?)?", Pattern.CASE_INSENSITIVE)
        };

        return com.phqb.crawler.Utils.tryMatch(patterns, info);
    }

    public static String extractRamType(String info) {
        final Pattern[] patterns = {
                Pattern.compile("((?:LP)?DDR\\dL?)", Pattern.CASE_INSENSITIVE)
        };

        return com.phqb.crawler.Utils.tryMatch(patterns, info);
    }

    public static String extractStorageAmount(String info) {
        final Pattern[] patterns = {
                Pattern.compile("(\\d+)\\s*([TG]B)?.+?(?:HDD|SSD)", Pattern.CASE_INSENSITIVE),
                Pattern.compile("(?:HDD|SSD).+?(\\d+)\\s*([TG]B)?", Pattern.CASE_INSENSITIVE)
        };

        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(info);
            if (matcher.find()) {
                String unit = matcher.group(2);
                if ("TB".equalsIgnoreCase(unit)) {
                    return String.valueOf(Integer.parseInt(matcher.group(1)) * 1000);
                } else {
                    return matcher.group(1);
                }
            }
        }

        return "";
    }

    public static void main(String[] args) {
        System.out.println(extractProductModel("Dell Precision M4800 i7-4800MQ RAM 8GB HDD 500GB FHD Quadro K1100M"));
    }
}
