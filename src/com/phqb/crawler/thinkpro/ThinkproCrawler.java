package com.phqb.crawler.thinkpro;

import com.phqb.crawler.Crawler;

public class ThinkproCrawler extends Crawler {

    private static final String NAME = "thinkpro";

    @Override
    public String getName() {
        return NAME;
    }

    public static void main(String[] args) throws Exception {
        ThinkproCrawler c = new ThinkproCrawler();
        c.crawl("https://thinkpro.vn/laptop.html");
    }
}
