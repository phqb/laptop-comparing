package com.phqb.crawler.thinkking;

import com.phqb.crawler.Crawler;

public class ThinkkingCrawler extends Crawler {

    private static final String NAME = "thinkking";

    @Override
    public String getName() {
        return NAME;
    }

    public static void main(String[] args) throws Exception {
        ThinkkingCrawler c = new ThinkkingCrawler();
        c.crawl("https://thinkking.vn/san-pham.html");
    }
}
