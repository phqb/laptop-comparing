package com.phqb.crawler;

import com.phqb.schema.product.Product;
import com.phqb.schema.productlist.ProductList;
import com.phqb.storage.dao.CrawledProductDao;
import com.phqb.storage.dao.ProductDao;
import com.phqb.storage.dto.CrawledProductDto;
import com.phqb.storage.dto.ProductDto;
import com.phqb.utils.Utils;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBResult;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Crawler {

    public abstract String getName();

    private final CrawledProductDao crawledProductDao = new CrawledProductDao();

    private final ProductDao productDao = new ProductDao();

    private Logger getLogger() {
        return Logger.getLogger(getClass().getName());
    }

    public CrawlerConfiguration getConfiguration() {
        return CrawlerConfigurations.CRAWLER_CONFIGURATION_NAME_MAP.get(getName());
    }

    private Map<String, String> additionalHeaders;

    public Map<String, String> getAdditionalHeaders() {
        if (additionalHeaders == null) {
            additionalHeaders = new HashMap<>();
            for (CrawlerConfiguration.Header header : getConfiguration().getAdditionalHeaders()) {
                additionalHeaders.put(header.getKey(), header.getValue());
            }
        }

        return additionalHeaders;
    }

    public boolean isProductListPage(String url) {
        return url.matches(getConfiguration().getProductListUrlPattern());
    }

    public boolean isProductPage(String url) {
        return url.matches(getConfiguration().getProductUrlPattern());
    }

    private List<ProductList.ProductListItem> crawlProductList(String url) throws SAXException, TransformerException, JAXBException, IOException, URISyntaxException, XMLStreamException {
        getLogger().log(Level.INFO, "Crawling product list page " + url);

        Utils.setLogLevel(Level.WARNING);
        String html = Utils.fetchWithTryFix(url, getAdditionalHeaders());
        Utils.setLogLevel(Level.INFO);

        StreamSource xsltSource = new StreamSource(
                new File(getClass().getResource(getConfiguration().getProductListUrlsExtractor()).toURI()));

        JAXBResult jaxbResult = Utils.transformToJAXB(xsltSource, Collections.emptyMap(), null, html, ProductList.class);

        ProductList productList = (ProductList) jaxbResult.getResult();
        return productList.getProducts();
    }

    private Product crawlProduct(ProductList.ProductListItem crawlingProduct) throws IOException, XMLStreamException, URISyntaxException, JAXBException, TransformerException, SAXException {
        getLogger().log(Level.INFO, "Crawling product page " + crawlingProduct.getUrl());

        Utils.setLogLevel(Level.WARNING);
        String html = Utils.fetchWithTryFix(crawlingProduct.getUrl(), getAdditionalHeaders());
        Utils.setLogLevel(Level.INFO);

        StreamSource xsltSource = new StreamSource(
                new File(getClass().getResource(getConfiguration().getProductExtractor()).toURI()));

        Map<String, String> params = new HashMap<>();
        if (crawlingProduct.getRawStatus() != null) {
            params.put("rawStatus", crawlingProduct.getRawStatus());
        }

        StreamSource xsdSource = new StreamSource(new File(getClass().getResource("/com/phqb/schema/product.xsd").toURI()));
        JAXBResult jaxbResult = Utils.transformToJAXB(xsltSource, params, xsdSource, html,
                "com.phqb.schema.product");

        Product product = (Product) jaxbResult.getResult();
        product.setUrl(crawlingProduct.getUrl());

        return product;
    }

    public void crawl(String startUrl) {
        Queue<ProductList.ProductListItem> crawQueue = new LinkedList<>();
        Set<String> crawledUrl = new HashSet<>();
        ProductList.ProductListItem startProductList = new ProductList.ProductListItem();
        startProductList.setUrl(startUrl);
        crawQueue.add(startProductList);

        while (!crawQueue.isEmpty()) {
            ProductList.ProductListItem crawlingProduct = crawQueue.poll();

            if (crawledUrl.contains(crawlingProduct.getUrl())) {
                continue;
            }

            if (isProductListPage(crawlingProduct.getUrl())) {
                try {
                    List<ProductList.ProductListItem> productList = crawlProductList(crawlingProduct.getUrl());
                    crawQueue.addAll(productList);
                } catch (Exception ignored) {
                }
            } else if (isProductPage(crawlingProduct.getUrl())) {
                try {
                    Product product = crawlProduct(crawlingProduct);

                    JAXBContext jaxbContext = JAXBContext.newInstance(Product.class);
                    Marshaller marshaller = jaxbContext.createMarshaller();
                    StringWriter stringWriter = new StringWriter();
                    marshaller.marshal(product, stringWriter);
                    stringWriter.flush();

                    CrawledProductDto crawledProductDto = new CrawledProductDto();
                    crawledProductDto.setUrl(product.getUrl());
                    crawledProductDto.setData(stringWriter.toString());
                    crawledProductDto.setCrawledAt(System.currentTimeMillis());
                    crawledProductDao.insert(crawledProductDto);

                    ProductDto productDto = com.phqb.schema.Utils.convertToProductDto(product);
                    productDao.upsert(productDto);
                } catch (Exception e) {
                    getLogger().log(
                            Level.WARNING,
                            "There is an error while crawling and validating data: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            crawledUrl.add(crawlingProduct.getUrl());
        }
    }
}
