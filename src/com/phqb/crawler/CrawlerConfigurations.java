package com.phqb.crawler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@XmlRootElement(name = "crawlers")
@XmlAccessorType(XmlAccessType.FIELD)
public class CrawlerConfigurations {

    private static final Logger LOGGER = Logger.getLogger(CrawlerConfigurations.class.getName());

    private static final String CONFIGURATIONS_FILE = "crawlers.xml";

    public static final Map<String, CrawlerConfiguration> CRAWLER_CONFIGURATION_NAME_MAP = new HashMap<>();

    static {
        URL crawlerConfigurationsFileURL = CrawlerConfigurations.class.getResource(CONFIGURATIONS_FILE);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CrawlerConfigurations.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            CrawlerConfigurations crawlerConfigurations =
                    (CrawlerConfigurations) unmarshaller.unmarshal(new File(crawlerConfigurationsFileURL.toURI()));

            for (CrawlerConfiguration c : crawlerConfigurations.getCrawlerConfigurationList()) {
                CRAWLER_CONFIGURATION_NAME_MAP.put(c.getName(), c);
            }
        } catch (JAXBException e) {
            LOGGER.log(Level.SEVERE, "Could not new JAXB instance", e);
        } catch (URISyntaxException e) {
            LOGGER.log(Level.WARNING, "Can not read " + CONFIGURATIONS_FILE);
        }

    }

    @XmlElement(name = "crawler")
    private List<CrawlerConfiguration> crawlerConfigurationList;

    public CrawlerConfigurations() {
    }

    public List<CrawlerConfiguration> getCrawlerConfigurationList() {
        return crawlerConfigurationList;
    }

    public void setCrawlerConfigurationList(List<CrawlerConfiguration> crawlerConfigurationList) {
        this.crawlerConfigurationList = crawlerConfigurationList;
    }

    @Override
    public String toString() {
        return "CrawlerConfigurations{" +
                "crawlerConfigurationList=" + crawlerConfigurationList +
                '}';
    }
}
