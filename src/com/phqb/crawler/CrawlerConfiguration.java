package com.phqb.crawler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "crawler", propOrder = {
        "name",
        "crawlerClass",
        "productListUrlPattern",
        "productUrlPattern",
        "productListUrlsExtractor",
        "productExtractor",
        "additionalHeaders"
})
public class CrawlerConfiguration {

    @XmlType(name = "header", propOrder = { "key", "value" })
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Header {

        @XmlElement
        private String key;
        @XmlElement
        private String value;

        public Header() {
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    @XmlElement
    private String name;
    @XmlElement
    private String crawlerClass;
    @XmlElement
    private String productListUrlPattern;
    @XmlElement
    private String productUrlPattern;
    @XmlElement
    private String productListUrlsExtractor;
    @XmlElement
    private String productExtractor;
    @XmlElement(name = "header")
    @XmlElementWrapper(name = "additionalHeaders")
    private List<Header> additionalHeaders;

    public CrawlerConfiguration() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCrawlerClass() {
        return crawlerClass;
    }

    public void setCrawlerClass(String crawlerClass) {
        this.crawlerClass = crawlerClass;
    }

    public String getProductListUrlPattern() {
        return productListUrlPattern;
    }

    public void setProductListUrlPattern(String productListUrlPattern) {
        this.productListUrlPattern = productListUrlPattern;
    }

    public String getProductUrlPattern() {
        return productUrlPattern;
    }

    public void setProductUrlPattern(String productUrlPattern) {
        this.productUrlPattern = productUrlPattern;
    }

    public String getProductListUrlsExtractor() {
        return productListUrlsExtractor;
    }

    public void setProductListUrlsExtractor(String productListUrlsExtractor) {
        this.productListUrlsExtractor = productListUrlsExtractor;
    }

    public String getProductExtractor() {
        return productExtractor;
    }

    public void setProductExtractor(String productExtractor) {
        this.productExtractor = productExtractor;
    }

    public List<Header> getAdditionalHeaders() {
        if (additionalHeaders == null) {
            additionalHeaders = new ArrayList<>();
        }
        return additionalHeaders;
    }

    public void setAdditionalHeaders(List<Header> additionalHeaders) {
        this.additionalHeaders = additionalHeaders;
    }
}
