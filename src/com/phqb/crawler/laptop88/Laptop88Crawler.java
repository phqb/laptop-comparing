package com.phqb.crawler.laptop88;

import com.phqb.crawler.Crawler;

public class Laptop88Crawler extends Crawler {

    private static final String NAME = "laptop88";

    @Override
    public String getName() {
        return NAME;
    }

    public static void main(String[] args) throws Exception {
        Crawler c = new Laptop88Crawler();
        c.crawl("https://laptop88.vn/laptop-cu/c2.html-1");
    }
}
