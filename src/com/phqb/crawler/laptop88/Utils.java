package com.phqb.crawler.laptop88;

import java.util.regex.Pattern;

public class Utils {

    public static String extractProductTitle(String title) {
        final Pattern[] patterns = {
                Pattern.compile("(?:"
                        + "laptop"
                        + "|" + Pattern.quote("laptop mới")
                        + "|" + Pattern.quote("laptop cũ")
                        + "|laptop gaming"
                        + "|(?=acer|asus|dell|thinkpad|lenovo|macbook|msi|hp|razer)"
                        + ")\\s*(.+)", Pattern.CASE_INSENSITIVE)
        };

        return com.phqb.crawler.Utils.tryMatch(patterns, title);
    }
}
