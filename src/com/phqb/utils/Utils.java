package com.phqb.utils;

import com.phqb.htmlfixer.HTMLAutoFixer;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBResult;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Utils {

    private static String encodeUrl(String url) {
        String encodedUrl;
        try {
            encodedUrl = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            encodedUrl = url;
        }
        return encodedUrl.replaceAll(":", "").replaceAll("/", "__");
    }

    public static List<String> fetchLines(String url) throws IOException {
        return fetchLines(url, Collections.emptyMap());
    }

    public static List<String> fetchLines(String url, Map<String, String> headers) throws IOException {
        boolean cached = true;
        InputStream is;
        HttpURLConnection conn;

        File cachedFile = new File("cache/" + encodeUrl(url));
        if (cachedFile.exists()) {
            conn = null;
            is = new FileInputStream(cachedFile);
        } else {
            cached = false;

            conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("GET");

            for (Map.Entry<String, String> header : headers.entrySet()) {
                conn.setRequestProperty(header.getKey(), header.getValue());
            }

            is = conn.getInputStream();
        }

        List<String> result = new ArrayList<>();

        try (InputStreamReader isr = new InputStreamReader(is);
             BufferedReader br = new BufferedReader(isr)) {
            if (!cached && (new File("cache").exists() || new File("cache").mkdir())) {
                FileWriter fileWriter = new FileWriter("cache/" + encodeUrl(url));

                String line;
                while ((line = br.readLine()) != null) {
                    result.add(line);
                    fileWriter.write(line);
                    fileWriter.write("\n");
                }

                fileWriter.flush();
                fileWriter.close();
            } else {
                String line;
                while ((line = br.readLine()) != null) {
                    result.add(line);
                }
            }
        } finally {
            is.close();
            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    public static List<String> fetchLinesWithTryFix(String url, Map<String, String> headers) throws IOException, XMLStreamException {
        return HTMLAutoFixer.tryFixAll(fetchLines(url, headers));
    }

    public static List<String> fetchLinesWithTryFix(String url) throws IOException, XMLStreamException {
        return HTMLAutoFixer.tryFixAll(fetchLines(url, Collections.emptyMap()));
    }

    public static String fetchWithTryFix(String url) throws IOException, XMLStreamException {
        return String.join("\n", fetchLinesWithTryFix(url));
    }

    public static String fetchWithTryFix(String url, Map<String, String> headers) throws IOException, XMLStreamException {
        return String.join("\n", fetchLinesWithTryFix(url, headers));
    }

    private static JAXBResult transformToJAXB(JAXBContext jaxbContext, StreamSource xsltSource, Map<String, String> params, StreamSource xsdSource, StreamSource xmlSource) throws TransformerException, JAXBException, SAXException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer(xsltSource);

        for (Map.Entry<String, String> param : params.entrySet()) {
            transformer.setParameter(param.getKey(), param.getValue());
        }

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        if (xsdSource != null) {
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(xsdSource);
            unmarshaller.setSchema(schema);
        }

        JAXBResult jaxbResult = new JAXBResult(unmarshaller);
        transformer.transform(xmlSource, jaxbResult);
        return jaxbResult;
    }

    private static JAXBResult transformToJAXB(JAXBContext jaxbContext, StreamSource xsltSource, Map<String, String> params, StreamSource xsdSource, String source) throws TransformerException, JAXBException, IOException, SAXException {
        ByteArrayInputStream sourceIs = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8));
        StreamSource htmlSource = new StreamSource(sourceIs);

        JAXBResult jaxbResult = transformToJAXB(jaxbContext, xsltSource, params, xsdSource, htmlSource);

        sourceIs.close();

        return jaxbResult;
    }

    public static JAXBResult transformToJAXB(StreamSource xsltSource, Map<String, String> params, StreamSource xsdSource, StreamSource xmlSource, Class classToBeBound) throws TransformerException, JAXBException, SAXException {
        return transformToJAXB(JAXBContext.newInstance(classToBeBound), xsltSource, params, xsdSource, xmlSource);
    }

    public static JAXBResult transformToJAXB(StreamSource xsltSource, Map<String, String> params, StreamSource xsdSource, String source, Class classToBeBound) throws IOException, TransformerException, JAXBException, SAXException {
        return transformToJAXB(JAXBContext.newInstance(classToBeBound), xsltSource, params, xsdSource, source);
    }

    public static JAXBResult transformToJAXB(StreamSource xsltSource, Map<String, String> params, StreamSource xsdSource, String source, String context) throws IOException, TransformerException, JAXBException, SAXException {
        return transformToJAXB(JAXBContext.newInstance(context), xsltSource, params, xsdSource, source);
    }

    public static void setLogLevel(Level level) {
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(level);
        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(level);
        }
    }

    public static String reflectionToString(Object object) {
        return reflectionToString(object, 0);
    }

    private static String reflectionToString(Object object, int depth) {
        if (object == null) {
            return "null";
        } else if (object instanceof Number || object instanceof Boolean || object instanceof String) {
            return object.toString();
        } else if (object instanceof Object[] || object instanceof Collection) {
            StringBuilder stringBuilder = new StringBuilder("[\n");

            Object[] items;
            if (object instanceof Object[]) {
                items = (Object[]) object;
            } else {
                items = ((Collection) object).toArray();
            }

            int index = 0;
            for (Object item : items) {
                for (int i = 0; i <= depth; i += 1) {
                    stringBuilder.append("    ");
                }
                stringBuilder.append(reflectionToString(item, depth + 1));
                if (index < items.length - 1) {
                    stringBuilder.append(",\n");
                }
                index += 1;
            }

            stringBuilder.append("\n");
            for (int i = 0; i < depth; i += 1) {
                stringBuilder.append("    ");
            }

            return stringBuilder.append("]").toString();
        } else {
            StringBuilder stringBuilder = new StringBuilder("{\n");

            Method[] methods = object.getClass().getMethods();

            int index = 0;
            for (Method method : methods) {
                String methodName = method.getName();

                if (!"getClass".equals(methodName) && methodName.matches("^(get|is)\\w+$")) {
                    String rawPropertyName = methodName.replaceFirst("^(get|is)", "");
                    String propertyName = rawPropertyName.substring(0, 1).toLowerCase()
                            .concat(rawPropertyName.substring(1));

                    try {
                        for (int d = 0; d <= depth; d += 1) {
                            stringBuilder.append("    ");
                        }

                        stringBuilder
                                .append(propertyName)
                                .append(": ")
                                .append(reflectionToString(method.invoke(object), depth + 1));

                        if (index < methods.length - 1) {
                            stringBuilder.append(",\n");
                        }
                    } catch (IllegalAccessException | InvocationTargetException ignored) {
                    }
                }
                index += 1;
            }

            for (int d = 0; d < depth; d += 1) {
                stringBuilder.append("    ");
            }

            return stringBuilder.append("}").toString();
        }
    }

    public static String removeHtmlMiscTags(String html) {
        return html
                .replaceAll("<script[^>]*>[\\s\\S]*?</script>", "")
                .replaceAll("<!--[\\s\\S]*?-->", "")
                .replaceAll("onclick=\"[^\"]*\"", "");
    }
    
    public static String localizeProductCompareCriteria(String criteria) {
        switch (criteria) {
            case "ConditionNewPercent":
                return "Phần trăm mới";
            case "Price":
                return "Giá";
            case "CpuPassmarkMark":
                return "Điểm CPU";
            case "RamInGB":
                return "Dung lượng RAM";
            case "StorageAmountInGB":
                return "Dung lượng ổ cứng";
            case "ScreenSizeInInch":
                return "Kích thước màn hình";
            case "ScreenResolutionWidth":
                return "Độ phân giải màn hình";
            case "GraphicCardMemoryInGB":
                return "Bộ nhớ card đồ họa";
            case "GraphicCardIntegratedOrDedicated":
                return "Card đồ họa tích hợp (1 điểm) hay rời (0 điểm)";
            default:
                return "";
        }
    }
}
