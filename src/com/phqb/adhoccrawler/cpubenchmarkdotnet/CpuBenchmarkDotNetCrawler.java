package com.phqb.adhoccrawler.cpubenchmarkdotnet;

import com.phqb.htmlfixer.HTMLAutoFixer;
import com.phqb.schema.cpubenchmarks.Cpus;
import com.phqb.storage.dao.CpuBenchmarkDao;
import com.phqb.storage.dto.CpuBenchmarkDto;
import com.phqb.utils.Utils;
import org.xml.sax.SAXException;

import javax.naming.NamingException;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBResult;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CpuBenchmarkDotNetCrawler {

    public static void main(String[] args) throws IOException, XMLStreamException, URISyntaxException, TransformerException, JAXBException, SAXException, SQLException, NamingException {
        List<String> lines = Utils.fetchLines("https://www.cpubenchmark.net/cpu_list.php");
        String joinedLines = String.join("\n", lines);
        int beginIndex = joinedLines.indexOf("<TABLE ID=\"cputable\" class=\"cpulist\">");
        int endIndex = joinedLines.indexOf("</TABLE>", beginIndex);
        String table = joinedLines.substring(beginIndex, endIndex + "</TABLE>".length());

        List<String> tableAsLines = Arrays.asList(table.split("\n"));
        List<String> fixedLines = HTMLAutoFixer.tryFixAll(tableAsLines);
        String joinedFixedLines = String.join("\n", fixedLines);

        StreamSource xsltSource = new StreamSource(new File(CpuBenchmarkDotNetCrawler.class.getResource("cpu_extractor.xsl").toURI()));
        StreamSource xsdSource = new StreamSource(new File(CpuBenchmarkDotNetCrawler.class.getResource("/com/phqb/schema/cpus.xsd").toURI()));

        JAXBResult jaxbResult = Utils.transformToJAXB(xsltSource, Collections.emptyMap(), xsdSource, joinedFixedLines, "com.phqb.schema.cpubenchmarks");
        Cpus benchmarks = (Cpus) jaxbResult.getResult();

        CpuBenchmarkDao cpuBenchmarkDao = new CpuBenchmarkDao();

        List<CpuBenchmarkDto> cpuBenchmarkDtos = benchmarks.getCpu().stream().map(com.phqb.schema.Utils::convertToCpuBenchmarkDto).collect(Collectors.toList());
        for (CpuBenchmarkDto cpuBenchmarkDto : cpuBenchmarkDtos) {
            cpuBenchmarkDao.upsert(cpuBenchmarkDto);
        }
    }
}
