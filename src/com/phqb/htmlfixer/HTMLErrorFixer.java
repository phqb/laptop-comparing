package com.phqb.htmlfixer;

import javax.xml.stream.Location;
import java.util.List;

public interface HTMLErrorFixer {

    HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines);
}
