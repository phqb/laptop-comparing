package com.phqb.htmlfixer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlRootElement(name = "resolvers")
@XmlAccessorType(XmlAccessType.FIELD)
class HTMLErrorFixerConfigurations {

    private static class PatternAndFixer {

        final Pattern pattern;
        final HTMLErrorFixer fixer;

        PatternAndFixer(Pattern pattern, HTMLErrorFixer fixer) {
            this.pattern = pattern;
            this.fixer = fixer;
        }
    }

    static class MatchesAndFixer {

        final Matcher matches;
        final HTMLErrorFixer fixer;

        MatchesAndFixer(Matcher matches, HTMLErrorFixer fixer) {
            this.matches = matches;
            this.fixer = fixer;
        }
    }

    private static final Logger LOGGER = Logger.getLogger(HTMLErrorFixerConfigurations.class.getName());

    private static final String FIXERS_RESOLVER_FILE = "resolvers.xml";

    private static final List<PatternAndFixer> patternAndFixers = new LinkedList<>();

    static {
        URL resolversUrl = HTMLErrorFixerConfigurations.class.getResource(FIXERS_RESOLVER_FILE);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(HTMLErrorFixerConfigurations.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            HTMLErrorFixerConfigurations rs =
                    (HTMLErrorFixerConfigurations) unmarshaller.unmarshal(new File(resolversUrl.toURI()));

            for (HTMLErrorFixerConfiguration fixerConf : rs.fixerList) {
                String pattern = fixerConf.getPattern();
                Pattern compiledPattern = Pattern.compile(pattern);

                String fixerClass = fixerConf.getFixerClass();
                Class<?> fixer = Class.forName(fixerClass);
                Constructor<?> fixerCtor = fixer.getConstructor();
                Object fixerInstance = fixerCtor.newInstance();

                if (fixerInstance instanceof HTMLErrorFixer) {
                    HTMLErrorFixer xmlFixerInstance = (HTMLErrorFixer) fixerInstance;
                    patternAndFixers.add(new PatternAndFixer(compiledPattern, xmlFixerInstance));
                } else {
                    Logger.getLogger(HTMLErrorFixerConfigurations.class.getName())
                            .log(Level.WARNING, "The fixer class " + fixerClass
                                    + " is not derived from " + HTMLErrorFixer.class.getName());
                }
            }
        } catch (URISyntaxException e) {
            LOGGER.log(Level.WARNING, "Can not read " + FIXERS_RESOLVER_FILE);
        } catch (JAXBException e) {
            LOGGER.log(Level.SEVERE, "Could not new JAXB instance", e);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.WARNING, "Can not find fixer", e);
        } catch (NoSuchMethodException | IllegalAccessException |
                InstantiationException | InvocationTargetException e) {
            LOGGER.log(Level.WARNING, "Can not instantiate fixer", e);
        }
    }

    static Optional<MatchesAndFixer> match(String errorMessage) {
        for (PatternAndFixer patternAndFixer : patternAndFixers) {
            Matcher matches = patternAndFixer.pattern.matcher(errorMessage);
            if (matches.find()) {
                return Optional.of(new MatchesAndFixer(matches, patternAndFixer.fixer));
            }
        }

        return Optional.empty();
    }

    @XmlElement(name = "resolver")
    private List<HTMLErrorFixerConfiguration> fixerList;
}
