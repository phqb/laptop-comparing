package com.phqb.htmlfixer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resolver", propOrder = {
        "pattern",
        "fixerClass"
})
public class HTMLErrorFixerConfiguration {

    @XmlElement(name = "pattern")
    private String pattern;

    @XmlElement(name = "fixerClass")
    private String fixerClass;

    public HTMLErrorFixerConfiguration() {
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getFixerClass() {
        return fixerClass;
    }

    public void setFixerClass(String fixerClass) {
        this.fixerClass = fixerClass;
    }
}
