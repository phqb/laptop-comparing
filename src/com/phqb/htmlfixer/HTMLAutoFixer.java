package com.phqb.htmlfixer;

import javax.xml.stream.Location;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;

public class HTMLAutoFixer {

    private static final XMLInputFactory XML_INPUT_FACTORY = XMLInputFactory.newInstance();
    private static final Logger LOGGER = Logger.getLogger(HTMLAutoFixer.class.getName());

    private static String getXMLErrorMessage(XMLStreamException exception) {
        return exception.getMessage().split("\n")[1].replaceAll("^Message:\\s*", "");
    }

    private static Optional<XMLStreamException> findFirstError(List<String> lines) throws XMLStreamException {
        InputStream linesInputStream =
                new ByteArrayInputStream(String.join("\n", lines).getBytes(StandardCharsets.UTF_8));
        XMLEventReader eventReader = XML_INPUT_FACTORY.createXMLEventReader(linesInputStream);

        while (eventReader.hasNext()) {
            try {
                eventReader.nextEvent();
            } catch (XMLStreamException e) {
                eventReader.close();
                return Optional.of(e);
            }
        }

        eventReader.close();
        return Optional.empty();
    }

    private static Optional<HTMLErrorFixingResult> tryFixFirst(List<String> lines) throws XMLStreamException {
        Optional<XMLStreamException> maybeError = findFirstError(lines);

        if (!maybeError.isPresent()) {
            return Optional.empty();
        }

        XMLStreamException error = maybeError.get();
        String errorMessage = getXMLErrorMessage(error);
        Location errorLocation = error.getLocation();

        LOGGER.log(Level.INFO, "XML Error at line " + errorLocation.getLineNumber() +
                " col " + errorLocation.getColumnNumber() + ": " + errorMessage);
        LOGGER.log(Level.INFO, lines.get(errorLocation.getLineNumber() - 1));

        Optional<HTMLErrorFixerConfigurations.MatchesAndFixer> maybeMatchesAndFixer =
                HTMLErrorFixerConfigurations.match(errorMessage);

        if (!maybeMatchesAndFixer.isPresent()) {
            LOGGER.log(Level.WARNING, "Could not find a fixer for error " + errorMessage);
            return Optional.empty();
        }

        HTMLErrorFixerConfigurations.MatchesAndFixer matchesAndFixer = maybeMatchesAndFixer.get();

        LOGGER.log(Level.INFO, "Trying to fix with " + matchesAndFixer.fixer.getClass().getName());

        Matcher matches = matchesAndFixer.matches;
        String[] errorMatches = new String[matches.groupCount()];
        for (int i = 0, n = matches.groupCount(); i < n; i += 1) {
            errorMatches[i] = matches.group(i + 1);
        }

        return Optional.of(matchesAndFixer.fixer.resolve(errorLocation, errorMatches, lines));
    }

    public static List<String> tryFixAll(List<String> lines) throws XMLStreamException {
//        // Remove misc tags
//        String joinedLine = String.join("\n", lines);
//        String stripedHtml = Utils.removeHtmlMiscTags(joinedLine);
//        lines = Arrays.asList(stripedHtml.split("\n"));

        Optional<HTMLErrorFixingResult> firstFixed = tryFixFirst(lines);

        // If could not fix anything or after fixing, nothing change, return the original lines and do nothing
        // Doing so prevent infinite loop when not fixing anything
        while (firstFixed.isPresent() && firstFixed.get().isChanged()) {
            firstFixed = tryFixFirst(lines);
        }

        return lines;
    }
}
