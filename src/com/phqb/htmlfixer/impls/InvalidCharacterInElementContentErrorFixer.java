package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InvalidCharacterInElementContentErrorFixer implements HTMLErrorFixer {

    private static final Logger LOGGER = Logger.getLogger(InvalidCharacterInElementContentErrorFixer.class.getName());

    public InvalidCharacterInElementContentErrorFixer() {
    }

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;
        String errorLine = lines.get(errorLineNum);
        String fixedLine;

        Matcher matcher = Pattern.compile("<([^>]+)>(.+)</[^>]+>").matcher(errorLine);
        if (matcher.find()) {
            String tagName = matcher.group(1);
            String content = matcher.group(2);
            String fixedContent = content.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            fixedLine = "<" + tagName + ">" + fixedContent + "</" + tagName + ">";
        } else {
            fixedLine = errorLine.replaceAll("<", "&lt;");
        }

        if (!errorLine.equals(fixedLine)) {
            LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
            lines.set(errorLineNum, fixedLine);
            LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");
            return new HTMLErrorFixingResult(true, lines);
        }

        return new HTMLErrorFixingResult(false, lines);
    }
}
