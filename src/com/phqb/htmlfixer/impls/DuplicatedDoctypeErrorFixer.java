package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DuplicatedDoctypeErrorFixer implements HTMLErrorFixer {

    private static final Logger LOGGER = Logger.getLogger(DuplicatedDoctypeErrorFixer.class.getName());

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;
        String errorLine = lines.get(errorLineNum);
        String fixedLine = errorLine.replaceAll("<!DOCTYPE[^>]*>", "");
        if (!errorLine.equals(fixedLine)) {
            LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
            lines.set(errorLineNum, fixedLine);
            LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");
            return new HTMLErrorFixingResult(true, lines);
        } else {
            return new HTMLErrorFixingResult(false, lines);
        }
    }
}
