package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class HTMLEntityNotDeclaredErrorFixer implements HTMLErrorFixer {

    private final Logger LOGGER = Logger.getLogger(HTMLEntityNotDeclaredErrorFixer.class.getName());

    public HTMLEntityNotDeclaredErrorFixer() {
    }

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;
        String errorLine = lines.get(errorLineNum);
        String entityRef = errorMatches[0];

        LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
        String fixedLine = errorLine.replaceFirst(
                "&" + Pattern.quote(entityRef) + ";",
                "&amp;" + entityRef + ";");
        LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");

        if (!errorLine.equals(fixedLine)) {
            lines.set(errorLineNum, fixedLine);
            return new HTMLErrorFixingResult(true, lines);
        } else {
            return new HTMLErrorFixingResult(false, lines);
        }
    }
}
