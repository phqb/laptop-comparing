package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class DuplicatedAttributesErrorFixer implements HTMLErrorFixer {

    private static final Logger LOGGER = Logger.getLogger(DuplicatedAttributesErrorFixer.class.getName());

    private static final int NUMBER_OF_LINES_TO_BACKTRACE = 1;

    public DuplicatedAttributesErrorFixer() {
    }

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;

        String errorAtt = errorMatches[1];
        String errorElem = errorMatches[0];

        for (int i = 0; i >= -NUMBER_OF_LINES_TO_BACKTRACE && errorLineNum + i >= 0; i -= 1) {
            String errorLine = lines.get(errorLineNum + i);
            LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
            String fixedLine = errorLine.replaceFirst(
                    "(<" + Pattern.quote(errorElem)
                            + "(?:\\s+[^=]+=\"[^\"]*\")*\\s+)"
                            + Pattern.quote(errorAtt)
                            + "=\"[^\"]*\"",
                    "$1");
            LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");

            if (!errorLine.equals(fixedLine)) {
                lines.set(errorLineNum + i, fixedLine);
                return new HTMLErrorFixingResult(true, lines);
            }
        }

        return new HTMLErrorFixingResult(false, lines);
    }
}
