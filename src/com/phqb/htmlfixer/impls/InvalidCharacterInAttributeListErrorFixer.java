package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InvalidCharacterInAttributeListErrorFixer implements HTMLErrorFixer {

    private static final Logger LOGGER = Logger.getLogger(InvalidCharacterInAttributeListErrorFixer.class.getName());

    public InvalidCharacterInAttributeListErrorFixer() {
    }

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;
        int errorColNum = errorLocation.getColumnNumber() - 1;
        String errorLine = lines.get(errorLineNum);

        LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
        String fixedLine = errorLine.substring(0, errorColNum).concat(errorLine.substring(errorColNum + 1));
        LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");

        if (!errorLine.equals(fixedLine)) {
            lines.set(errorLineNum, fixedLine);
            return new HTMLErrorFixingResult(true, lines);
        } else {
            return new HTMLErrorFixingResult(false, lines);
        }
    }
}
