package com.phqb.htmlfixer.impls;

import com.phqb.htmlfixer.HTMLErrorFixer;
import com.phqb.htmlfixer.HTMLErrorFixingResult;

import javax.xml.stream.Location;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MismatchedEndTagErrorFixer implements HTMLErrorFixer {

    private static final Set<String> EMPTY_TAGS = new HashSet<>();

    private static final Set<String> NOT_DANGLING_CLOSE_TAGS = new HashSet<>();

    static {
        EMPTY_TAGS.add("meta");
        EMPTY_TAGS.add("img");
        EMPTY_TAGS.add("input");
        EMPTY_TAGS.add("br");
        EMPTY_TAGS.add("link");
        EMPTY_TAGS.add("hr");
        NOT_DANGLING_CLOSE_TAGS.add("body");
        NOT_DANGLING_CLOSE_TAGS.add("html");
    }

    private final Logger LOGGER = Logger.getLogger(MismatchedEndTagErrorFixer.class.getName());

    public MismatchedEndTagErrorFixer() {
    }

    @Override
    public HTMLErrorFixingResult resolve(Location errorLocation, String[] errorMatches, List<String> lines) {
        int errorLineNum = errorLocation.getLineNumber() - 1;
        String startTag = errorMatches[0].toLowerCase();

        if (EMPTY_TAGS.contains(startTag)) {
            for (int i = errorLineNum; i >= 0; i -= 1) {
                String errorLine = lines.get(i);
                String fixedLine = errorLine.replaceFirst(
                        "<" + Pattern.quote(startTag)
                                + "([^>]*[^/])>|<"
                                + Pattern.quote(startTag)
                                + ">",
                        "<" + startTag + "$1/>");

                if (!fixedLine.equals(errorLine)) {
                    LOGGER.log(Level.INFO, "Before fixing: [" + lines.get(i) + "]");
                    LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");
                    lines.set(i, fixedLine);
                    return new HTMLErrorFixingResult(true, lines);
                }
            }
        } else {
            String errorLine = lines.get(errorLineNum);
            Matcher match = Pattern.compile("([^>]+)").matcher(errorLine);

            if (match.find(errorLocation.getColumnNumber() - 1)) {
                String mismatchedEndTag = match.group(1);

                String fixedLine;
                if (NOT_DANGLING_CLOSE_TAGS.contains(mismatchedEndTag)) {
                    fixedLine = errorLine.replaceFirst(
                            "</" + Pattern.quote(mismatchedEndTag) + ">",
                            "</" + startTag + "></" + mismatchedEndTag + ">");
                } else {
                    fixedLine = errorLine.replaceFirst(
                            "</" + Pattern.quote(mismatchedEndTag) + ">",
                            "");
                }

                LOGGER.log(Level.INFO, "Before fixing: [" + errorLine + "]");
                LOGGER.log(Level.INFO, "After fixing:  [" + fixedLine + "]");

                if (!errorLine.equals(fixedLine)) {
                    lines.set(errorLineNum, fixedLine);
                    return new HTMLErrorFixingResult(true, lines);
                }
            } else {
                LOGGER.log(Level.WARNING, "Could not find mismatched end-tag");
            }
        }

        return new HTMLErrorFixingResult(false, lines);
    }
}
