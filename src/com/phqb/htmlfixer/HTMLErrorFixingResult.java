package com.phqb.htmlfixer;

import java.util.List;

public class HTMLErrorFixingResult {

    private final boolean changed;

    private final List<String> fixedLines;

    public HTMLErrorFixingResult(boolean changed, List<String> fixedLines) {
        this.changed = changed;
        this.fixedLines = fixedLines;
    }

    public boolean isChanged() {
        return changed;
    }

    public List<String> getFixedLines() {
        return fixedLines;
    }
}
