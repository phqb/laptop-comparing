package com.phqb.playground;

import com.phqb.utils.Utils;

import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

public class Main {

    public static void main(String[] args) throws Exception {
        String html = Utils.fetchWithTryFix("https://www.cpubenchmark.net/cpu_list.php");
        try (FileOutputStream fos = new FileOutputStream("cpu-list.xml")) {
            fos.write(html.getBytes(StandardCharsets.UTF_8));
            fos.flush();
        }
//        String crawlingUrl = "https://laptop88.vn/laptop-dell-inspiron-5557-core-i5-6200u-ram-4-hdd-500gb-nvidia-geforce-930m-hd-156-inch-bh-6-thang/p2544.html";
//        String fileName = crawlingUrl.replaceAll("^http(s)?://", "").replaceAll("/", "_").replaceAll("\\.html$", ".xml");
//
//        String html = Utils.fetchWithTryFix(crawlingUrl);
//        ByteArrayInputStream htmlIs = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
//        StreamSource xmlSource = new StreamSource(htmlIs);
//        StreamSource xsltSource = new StreamSource(new File("src/com/phqb/crawler/laptop88/laptop88_product_extractor.xsl"));
//
//        try (FileWriter fileWriter = new FileWriter(new File(fileName))) {
//            StreamResult streamResult = new StreamResult(fileWriter);
//            TransformerFactory tf = TransformerFactory.newInstance();
//            Transformer transformer = tf.newTransformer(xsltSource);
//            transformer.transform(xmlSource, streamResult);
//            fileWriter.flush();
//        }
    }
}
