package com.phqb.api;

import com.phqb.storage.dao.ProductDao;

import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("search_keywords")
public class SearchKeywordsService {

    private static final Logger LOGGER = Logger.getLogger(SearchKeywordsService.class.getName());

    private static final long CACHE_DURATION = 24 * 60 * 60 * 1000;
    private static List<String> cachedKeywords;
    private static long lastCached;

    private final ProductDao productDao = new ProductDao();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getSearchKeywords() throws SQLException, NamingException {
        long currentTime = System.currentTimeMillis();

        if (cachedKeywords == null || currentTime - lastCached > CACHE_DURATION) {
            LOGGER.log(Level.INFO, "Cache miss or expired");

            synchronized (SearchKeywordsService.class) {
                if (cachedKeywords == null || currentTime - lastCached > CACHE_DURATION) {
                    lastCached = System.currentTimeMillis();
                    cachedKeywords = productDao.getSearchKeywords();
                }
            }
        } else {
            LOGGER.log(Level.INFO, "Cache hit");
        }

        return cachedKeywords;
    }
}
