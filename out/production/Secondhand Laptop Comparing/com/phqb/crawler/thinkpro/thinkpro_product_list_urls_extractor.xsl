<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="//div[contains(@class, 'product-list')]" mode="productList"/>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'product-list')]" mode="productList">
        <products>
            <xsl:apply-templates select="//div[contains(@class, 'product-item-inner')]" mode="productItemOrList"/>
        </products>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'product-item-inner')]" mode="productItemOrList">
        <product>
            <url>
                <xsl:text>https://thinkpro.vn</xsl:text>
                <xsl:value-of select="normalize-space(a/@href)"/>
            </url>
            <rawStatus>
                <xsl:value-of select="normalize-space(//span[contains(@class, 'sticky-left')])"/>
            </rawStatus>
        </product>
    </xsl:template>
</xsl:stylesheet>