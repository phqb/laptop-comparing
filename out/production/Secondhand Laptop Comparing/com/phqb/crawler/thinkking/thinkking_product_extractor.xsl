<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:java="http://xml.apache.org/xalan/java"
                exclude-result-prefixes="java"
                xmlns="phqb.com/product">
    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="quot">"</xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="//div[@id = 'product-detail']" mode="productDetail"/>
    </xsl:template>

    <xsl:template match="//div[@id = 'product-detail']" mode="productDetail">
        <xsl:variable name="productTitle">
            <xsl:value-of select="normalize-space(//a[@class = 'title']/h1)"/>
        </xsl:variable>

        <xsl:variable name="brand">
            <xsl:value-of select="substring-before($productTitle, ' ')"/>
        </xsl:variable>

        <xsl:variable name="rawCondition">
            <xsl:value-of select="//div[@class = 'product-info']/ul/li[2]/text()[1]"/>
        </xsl:variable>

        <product>
            <url/>
            <condition>
                <xsl:choose>
                    <xsl:when test="contains($rawCondition, 'Mới')">
                        <isNew>true</isNew>
                        <newPercent>100</newPercent>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="newPercent">
                            <xsl:value-of select="translate($rawCondition, translate($rawCondition, '0123456789', ''), '')"/>
                        </xsl:variable>
                        <xsl:choose>
                            <xsl:when test="string-length($newPercent) > 0">
                                <isNew>false</isNew>
                                <xsl:choose>
                                    <xsl:when test="string-length($newPercent) > 2">
                                        <newPercent><xsl:value-of select="substring(string($newPercent), 1, 2)"/></newPercent>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <newPercent><xsl:value-of select="$newPercent"/></newPercent>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <isNew>false</isNew>
                                <newPercent xsi:nil="true"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </condition>
            <brand>
                <xsl:value-of select="$brand"/>
            </brand>
            <model>
                <xsl:value-of select="java:com.phqb.crawler.Utils.extractProductModel($productTitle)"/>
            </model>
            <xsl:variable name="price">
                <xsl:value-of select="normalize-space(translate(//div[@class = 'product-info']/div[@class = 'price']/price[@class = 'last_price'], '.', ''))"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$price > 0">
                    <price>
                        <xsl:value-of select="$price"/>
                    </price>
                </xsl:when>
                <xsl:otherwise>
                    <price xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="rawStatus">
                <xsl:value-of select="normalize-space(//div[@class = 'product-info']/div[@class = 'price']/div[contains(@class, 'sticky')])"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$rawStatus = 'HẾT HÀNG'">
                    <status>
                        <xsl:text>out_of_stock</xsl:text>
                    </status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'CÒN HÀNG' or $rawStatus = 'SẴN HÀNG' or $rawStatus = 'MỚI VỀ'">
                    <status>
                        <xsl:text>in_stock</xsl:text>
                    </status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'SẮP VỀ'">
                    <status>
                        <xsl:text>coming_soon</xsl:text>
                    </status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'LIÊN HỆ'">
                    <status>
                        <xsl:text>contact</xsl:text>
                    </status>
                </xsl:when>
                <xsl:when test="contains($rawStatus, 'CHIẾC DUY NHẤT')">
                    <status>
                        <xsl:value-of select="substring-before($rawStatus, ' ')"/>
                    </status>
                </xsl:when>
                <xsl:otherwise>
                    <status xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <availableAt>
                <xsl:call-template name="LocationsSplit">
                    <xsl:with-param name="locations" select="//div[@class = 'product-info']/ul[contains(@class, 'field')]/li[4]/label/following-sibling::text()[1]"/>
                </xsl:call-template>
            </availableAt>
            <cpuTechnology>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-1']//div[@class = 'row'])[1]/div[2])"/>
            </cpuTechnology>
            <cpuType>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-1']//div[@class = 'row'])[2]/div[2])"/>
            </cpuType>
            <cpuSpeed>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-1']//div[@class = 'row'])[3]/div[2])"/>
            </cpuSpeed>
            <cpuCache>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-1']//div[@class = 'row'])[4]/div[2])"/>
            </cpuCache>
            <xsl:variable name="rawRAMAmount">
                <xsl:value-of select="(//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-2']//div[@class = 'row'])[1]/div[2]"/>
            </xsl:variable>
            <ramInGB>
                <xsl:value-of select="translate($rawRAMAmount, translate($rawRAMAmount, '0123456789', ''), '')"/>
            </ramInGB>
            <ramType>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-2']//div[@class = 'row'])[2]/div[2])"/>
            </ramType>
            <ramBus>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-2']//div[@class = 'row'])[3]/div[2])"/>
            </ramBus>
            <storageType>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-3']//div[@class = 'row'])[2]/div[2])"/>
            </storageType>
            <xsl:variable name="rawStorageAmount">
                <xsl:value-of select="(//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-3']//div[@class = 'row'])[1]/div[2]"/>
            </xsl:variable>
            <storageAmountInGB>
                <xsl:value-of select="translate($rawStorageAmount, translate($rawStorageAmount, '0123456789', ''), '')"/>
            </storageAmountInGB>
            <xsl:variable name="rawScreenSize">
                <xsl:value-of select="(//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[1]/div[2]"/>
            </xsl:variable>
            <xsl:variable name="screenSize">
                <xsl:choose>
                    <xsl:when test="contains($rawScreenSize, 'inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, 'Inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'Inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, $quot)">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, $quot))"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="string-length($screenSize) > 0">
                    <screenSizeInInch>
                        <xsl:value-of select="translate(translate($screenSize, translate($screenSize, '0123456789,.', ''), ''), ',', '.')"/>
                    </screenSizeInInch>
                </xsl:when>
                <xsl:otherwise>
                    <screenSizeInInch xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <screenResolution>
                <xsl:variable name="rawScreenDimension">
                    <xsl:value-of select="(//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[2]/div[2]"/>
                </xsl:variable>
                <xsl:variable name="rawWidth">
                    <xsl:value-of select="substring-before($rawScreenDimension, 'x')"/>
                </xsl:variable>
                <xsl:variable name="rawHeight">
                    <xsl:value-of select="substring-after($rawScreenDimension, 'x')"/>
                </xsl:variable>
                
                <type>
                    <xsl:value-of select="normalize-space(translate($rawScreenDimension, '0123456789()x', ''))"/>
                </type>
                <xsl:variable name="width">
                    <xsl:value-of select="translate($rawWidth, translate($rawWidth, '0123456789', ''), '')"/>
                </xsl:variable>
                <xsl:variable name="height">
                    <xsl:value-of select="translate($rawHeight, translate($rawHeight, '0123456789', ''), '')"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($width) > 0">
                        <width>
                            <xsl:value-of select="$width"/>
                        </width>
                    </xsl:when>
                    <xsl:otherwise>
                        <width xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="string-length($height) > 0">
                        <height>
                            <xsl:value-of select="$height"/>
                        </height>
                    </xsl:when>
                    <xsl:otherwise>
                        <height xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
            </screenResolution>
            <screenTechnology>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[3]/div[2])"/>
            </screenTechnology>
            <touchScreen>
                <xsl:variable name="rawTouchScreen">
                    <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[4]/div[2])"/>
                </xsl:variable>
                <xsl:if test="$rawTouchScreen = 'Không'">
                    <xsl:text>false</xsl:text>
                </xsl:if>
                <xsl:if test="not($rawTouchScreen = 'Không')">
                    <xsl:text>true</xsl:text>
                </xsl:if>
            </touchScreen>
            <graphicCard>
                <name>
                    <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[5]/div[2])"/>
                </name>
                <xsl:variable name="rawMemory">
                    <xsl:value-of select="normalize-space(substring-before(((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[6]/div[2]), 'GB'))"/>
                </xsl:variable>
                <xsl:variable name="memory">
                    <xsl:value-of select="normalize-space(substring-before(translate($rawMemory, 'GB', 'gb'), 'gb'))"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($memory) > 0">
                        <memoryInGB>
                            <xsl:value-of select="translate($memory, ',', '.')"/>
                        </memoryInGB>
                    </xsl:when>
                    <xsl:otherwise>
                        <memoryInGB xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                <integratedOrDedicated>
                    <xsl:variable name="rawIntegratedOrDedicated">
                        <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-4']//div[@class = 'row'])[7]/div[2])"/>
                    </xsl:variable>
                    <xsl:if test="$rawIntegratedOrDedicated = 'Onboard'">
                        <xsl:text>true</xsl:text>
                    </xsl:if>
                    <xsl:if test="not($rawIntegratedOrDedicated = 'Onboard')">
                        <xsl:text>false</xsl:text>
                    </xsl:if>
                </integratedOrDedicated>
            </graphicCard>
            <batteryInformation>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-5']//div[@class = 'row'])[1]/div[2])"/>
            </batteryInformation>
            <batteryLifeInHour>
                <xsl:variable name="rawBatteryLife">
                    <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-5']//div[@class = 'row'])[2]/div[2])"/>
                </xsl:variable>
                <xsl:variable name="rawMin">
                    <xsl:value-of select="normalize-space(substring-before($rawBatteryLife, '-'))"/>
                </xsl:variable>
                <xsl:variable name="rawMax">
                    <xsl:value-of select="normalize-space(substring-before(substring-after($rawBatteryLife, '-'), 'h'))"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($rawMin) > 0">
                        <min>
                            <xsl:value-of select="$rawMin"/>
                        </min>
                    </xsl:when>
                    <xsl:otherwise>
                        <min xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="string-length($rawMax) > 0">
                        <max>
                            <xsl:value-of select="$rawMax"/>
                        </max>
                    </xsl:when>
                    <xsl:otherwise>
                        <max xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
            </batteryLifeInHour>
            <audioTechnology>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-6']//div[@class = 'row'])[1]/div[2])"/>
            </audioTechnology>
            <webcamResolution>
                <xsl:value-of select="normalize-space((//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-6']//div[@class = 'row'])[3]/div[2])"/>
            </webcamResolution>
            <weightInKg>
                <xsl:variable name="rawWeight">
                    <xsl:value-of select="(//div[@class = 'product-extrainfo']//div[@id = 'tech']//div[@id = 'collapse-6']//div[@class = 'row'])[5]/div[2]"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="contains(translate($rawWeight, 'KG', 'kg'), 'kg')">
                        <xsl:value-of select="normalize-space(translate(substring-before(translate($rawWeight, 'KG', 'kg'), 'kg'), ',', '.'))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="translate(translate($rawWeight, translate($rawWeight, '0123456789.,', ''), ''), ',', '.')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </weightInKg>
        </product>
    </xsl:template>

    <xsl:template name="LocationsSplit">
        <xsl:param name="locations"/>
        <xsl:param name="delimiter">&amp;</xsl:param>
        <xsl:choose>
            <xsl:when test="contains($locations, $delimiter)">
                <location xmlns="phqb.com/product">
                    <xsl:value-of select="normalize-space(substring-before($locations, $delimiter))"/>
                </location>
                <xsl:call-template name="LocationsSplit">
                    <xsl:with-param name="locations" select="substring-after($locations, $delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="not(contains($locations, $delimiter))">
                <location xmlns="phqb.com/product">
                    <xsl:value-of select="normalize-space($locations)"/>
                </location>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>