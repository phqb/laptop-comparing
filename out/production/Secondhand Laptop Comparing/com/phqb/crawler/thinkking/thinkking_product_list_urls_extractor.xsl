<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="//div[contains(@class, 'product-list')]" mode="productList"/>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'product-list')]" mode="productList">
        <products>
            <xsl:apply-templates select="//div[contains(@class, 'product-item')]" mode="productItem"/>
            <xsl:apply-templates select="//ul[@class = 'pagination']/li/a[@href]" mode="productPageUrl"/>
        </products>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'product-item')]" mode="productItem">
        <product>
            <url>
                <xsl:text>https://thinkking.vn</xsl:text>
                <xsl:value-of select="normalize-space(a/@href)"/>
            </url>
            <rawStatus/>
        </product>
    </xsl:template>

    <xsl:template match="//ul[@class = 'pagination']/li/a[@href]" mode="productPageUrl">
        <product>
            <url>
                <xsl:value-of select="normalize-space(@href)"/>
            </url>
            <rawStatus/>
        </product>
    </xsl:template>
</xsl:stylesheet>