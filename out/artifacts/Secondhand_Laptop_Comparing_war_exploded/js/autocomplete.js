function autocomplete(inputElement, items, maxNumberOfSuggestions = 5) {
    const suggestionListId = inputElement.id + '__autocomplete-list';
    let selectedSuggestionItemIndex;
    let isSuggestionListShowing = false;

    inputElement.addEventListener('input', function (e) {
        closeSuggestionList();

        let inputValue = e.target.value;

        if (!inputValue) {
            return false;
        }

        selectedSuggestionItemIndex = -1;

        let suggestionList = document.createElement('DIV');
        suggestionList.setAttribute('id', suggestionListId);
        suggestionList.setAttribute('class', 'autocomplete-items');
        inputElement.parentNode.appendChild(suggestionList);

        let numberOfAddedSuggestions = 0;
        items.forEach((item) => {
            if (numberOfAddedSuggestions < maxNumberOfSuggestions
                && item.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0) {
                let suggestionItem = document.createElement('div');
                suggestionItem.append(document.createTextNode(item));

                suggestionItem.addEventListener('click', () => {
                    inputElement.value = item;
                    closeSuggestionList();
                });

                suggestionList.appendChild(suggestionItem);

                numberOfAddedSuggestions += 1;
            }
        });

        if (numberOfAddedSuggestions > 0) {
            isSuggestionListShowing = true;
        }
    });

    inputElement.addEventListener('keydown', function (keyEvent) {
        let suggestionListElement = document.getElementById(suggestionListId);
        if (!suggestionListElement) {
            return;
        }

        let suggestionItems = suggestionListElement.getElementsByTagName('div');

        if (keyEvent.keyCode === 40) {
            selectedSuggestionItemIndex++;
            addActive(suggestionItems);
        } else if (keyEvent.keyCode === 38) {
            selectedSuggestionItemIndex--;
            addActive(suggestionItems);
        } else if (keyEvent.keyCode === 13 && isSuggestionListShowing) {
            keyEvent.preventDefault();
            if (selectedSuggestionItemIndex > -1) {
                suggestionItems[selectedSuggestionItemIndex].click();
            }
        }
    });

    function addActive(suggestionItems) {
        if (!suggestionItems) {
            return false;
        }

        removeActive(suggestionItems);

        selectedSuggestionItemIndex = (selectedSuggestionItemIndex + suggestionItems.length) % suggestionItems.length;
        suggestionItems[selectedSuggestionItemIndex].classList.add('autocomplete-active');
    }

    function removeActive(suggestionItems) {
        Array.from(suggestionItems).forEach((item) => {
            item.classList.remove('autocomplete-active');
        });
    }

    function closeSuggestionList() {
        let suggestionList = document.getElementById(suggestionListId);
        if (suggestionList) {
            suggestionList.parentNode.removeChild(suggestionList);
            isSuggestionListShowing = false;
        }
    }

    document.addEventListener('click', closeSuggestionList);
}