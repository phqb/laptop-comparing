<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="//div[contains(@class, 'product-list')]" mode="productList"/>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'product-list')]" mode="productList">
        <products>
            <xsl:apply-templates select="//div[contains(@class, 'p-item')]" mode="productItem"/>
            <xsl:apply-templates select="//li[contains(@class, 'page-item')]" mode="productPageUrl"/>
        </products>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'p-item')]" mode="productItem">
        <product>
            <url>
                <xsl:text>https://laptop88.vn</xsl:text>
                <xsl:value-of select="normalize-space(.//a[@class = 'p-name']/@href)"/>
            </url>
            <rawStatus/>
        </product>
    </xsl:template>

    <xsl:template match="//li[contains(@class, 'page-item')]" mode="productPageUrl">
        <product>
            <url>
                <xsl:text>https://laptop88.vn</xsl:text>
                <xsl:value-of select="normalize-space(.//a[@class = 'page-link']/@href)"/>
            </url>
            <rawStatus/>
        </product>
    </xsl:template>
</xsl:stylesheet>