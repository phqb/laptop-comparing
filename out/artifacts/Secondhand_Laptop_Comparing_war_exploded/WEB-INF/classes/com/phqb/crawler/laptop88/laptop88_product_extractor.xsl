<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xalan/java"
                exclude-result-prefixes="java"
                version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:param name="rawStatus"/>

    <xsl:variable name="quot">"</xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="//div[contains(@class, 'detail-type')]" mode="productDetail"/>
    </xsl:template>

    <xsl:template match="//div[contains(@class, 'detail-type')]" mode="productDetail">
        <xsl:variable name="rawTitle">
            <xsl:value-of select="normalize-space(.//h1)"/>
        </xsl:variable>
        <xsl:variable name="productTitle">
            <xsl:value-of select="java:com.phqb.crawler.laptop88.Utils.extractProductTitle($rawTitle)"/>
        </xsl:variable>
        <xsl:variable name="rawProductConfig">
            <xsl:value-of select="substring-before(substring-after(//script[contains(text(), 'PRODUCT_ID')], 'var product_config = {'), '};')"/>
        </xsl:variable>
        <xsl:variable name="rawProductInfo">
            <xsl:value-of select="substring-before(substring-after($rawProductConfig, 'product_info'), '}')"/>
        </xsl:variable>

        <product xmlns="phqb.com/product"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <url/>
            <condition>
                <xsl:variable name="rawCondition1">
                    <xsl:value-of select="normalize-space(substring-before(substring-after($rawTitle, '['), ']'))"/>
                </xsl:variable>
                <xsl:variable name="rawCondition2">
                    <xsl:if test="contains($rawTitle, 'Cũ') or contains($rawTitle, 'cũ')">Cũ</xsl:if>
                </xsl:variable>

                <xsl:choose>
                    <xsl:when test="string-length($rawCondition1) > 0">
                        <xsl:variable name="newPercent">
                            <xsl:value-of select="translate($rawCondition1, translate($rawCondition1, '0123456789', ''), '')"/>
                        </xsl:variable>
                        <xsl:choose>
                            <xsl:when test="$newPercent = '100'">
                                <isNew>true</isNew>
                                <newPercent><xsl:value-of select="$newPercent"/></newPercent>
                            </xsl:when>
                            <xsl:otherwise>
                                <isNew>false</isNew>
                                <newPercent><xsl:value-of select="$newPercent"/></newPercent>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="string-length($rawCondition2) > 0">
                        <isNew>false</isNew>
                        <newPercent xsi:nil="true"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <isNew>false</isNew>
                        <newPercent xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condition>
            <brand>
                <xsl:value-of select="java:com.phqb.crawler.Utils.extractProductBrand($productTitle)"/>
            </brand>
            <model>
                <xsl:value-of select="java:com.phqb.crawler.Utils.extractProductModel($productTitle)"/>
            </model>
            <xsl:variable name="rawPrice">
                <xsl:value-of select="substring-before(substring-after($rawProductInfo, 'sale_price'), ',')"/>
            </xsl:variable>
            <xsl:variable name="price">
                <xsl:value-of select="translate($rawPrice, translate($rawPrice, '0123456789', ''), '')"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$price > 0">
                    <price>
                        <xsl:value-of select="$price"/>
                    </price>
                </xsl:when>
                <xsl:otherwise>
                    <price xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <status>
                <xsl:variable name="rawStockQuantity">
                    <xsl:value-of select="substring-before(substring-after($rawProductInfo, 'stock_quantity'), ',')"/>
                </xsl:variable>
                <xsl:variable name="stockQuantity">
                    <xsl:value-of select="translate($rawStockQuantity, translate($rawStockQuantity, '0123456789', ''), '')"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$stockQuantity > 0">
                        <xsl:value-of select="$stockQuantity"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>out_of_stock</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </status>
            <availableAt>
                <location>TP Hồ Chí Minh</location>
            </availableAt>
            <xsl:variable name="rawCpuInfo">
                <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'cpu', $quot)), '},')"/>
            </xsl:variable>
            <xsl:variable name="rawCpuValues">
                <xsl:value-of select="substring-before(substring-after($rawCpuInfo, concat($quot, 'values', $quot)), '],')"/>
            </xsl:variable>
            <cpuTechnology xsi:nil="true"/>
            <cpuType>
                <xsl:variable name="maybeCpuType">
                    <xsl:value-of select="normalize-space(substring-before(substring-after($rawCpuValues, concat($quot, 'label', $quot, ': ', $quot)), concat($quot, ',')))"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($maybeCpuType) > 0">
                        <xsl:value-of select="$maybeCpuType"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="maybeCpuType2">
                            <xsl:value-of select=".//div[@class = 'prodetail-spec-full']//tr[2]/td[2]"/>
                        </xsl:variable>
                        <xsl:choose>
                            <xsl:when test="string-length($maybeCpuType2) > 0">
                                <xsl:value-of select="$maybeCpuType2"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="java:com.phqb.crawler.Utils.extractCpuName($productTitle)"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </cpuType>
            <cpuSpeed xsi:nil="true"/>
            <cpuCache xsi:nil="true"/>
            <xsl:choose>
                <xsl:when test="contains($rawProductConfig, concat($quot, 'bo-nho', $quot))">
                    <xsl:variable name="combinedInfo">
                        <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'bo-nho', $quot)), '},')"/>
                    </xsl:variable>
                    <ramInGB>
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamAmount($combinedInfo)"/>
                    </ramInGB>
                    <ramType>
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamType($combinedInfo)"/>
                    </ramType>
                    <ramBus xsi:nil="true"/>
                    <storageType>
                        <xsl:if test="contains($combinedInfo, 'SSD')">
                            <xsl:text>SSD</xsl:text>
                        </xsl:if>
                        <xsl:if test="contains($combinedInfo, 'HDD')">
                            <xsl:text>HDD</xsl:text>
                        </xsl:if>
                    </storageType>
                    <storageAmountInGB>
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractStorageAmount($combinedInfo)"/>
                    </storageAmountInGB>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="rawRAMInfo">
                        <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'ram', $quot)), '},')"/>
                    </xsl:variable>
                    <xsl:variable name="ramAmount">
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamAmount($rawRAMInfo)"/>
                    </xsl:variable>
                    <ramInGB>
                        <xsl:choose>
                            <xsl:when test="string-length(normalize-space($ramAmount)) > 0">
                                <xsl:value-of select="normalize-space($ramAmount)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="ramAmount2">
                                    <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamAmount(.//div[@class = 'prodetail-spec-full'])"/>
                                </xsl:variable>
                                <xsl:choose>
                                    <xsl:when test="string-length(normalize-space($ramAmount2)) > 0">
                                        <xsl:value-of select="$ramAmount2"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamAmount($productTitle)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </ramInGB>
                    <ramType>
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractRamType($rawRAMInfo)"/>
                    </ramType>
                    <ramBus xsi:nil="true"/>
                    <xsl:variable name="rawStorageInfo">
                        <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'o-cung', $quot)), '},')"/>
                    </xsl:variable>
                    <storageType>
                        <xsl:choose>
                            <xsl:when test="contains($rawStorageInfo, 'SSD')">
                                <xsl:text>SSD</xsl:text>
                            </xsl:when>
                            <xsl:when test="contains($rawStorageInfo, 'HDD')">
                                <xsl:text>HDD</xsl:text>
                            </xsl:when>
                        </xsl:choose>
                    </storageType>
                    <xsl:variable name="storageAmount">
                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractStorageAmount($rawStorageInfo)"/>
                    </xsl:variable>
                    <storageAmountInGB>
                        <xsl:choose>
                            <xsl:when test="string-length(normalize-space($storageAmount)) > 0">
                                <xsl:value-of select="normalize-space($storageAmount)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="storageAmount2">
                                    <xsl:value-of select="java:com.phqb.crawler.Utils.extractStorageAmount(.//div[@class = 'prodetail-spec-full'])"/>
                                </xsl:variable>
                                <xsl:choose>
                                    <xsl:when test="string-length($storageAmount2) > 0">
                                        <xsl:value-of select="$storageAmount2"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="java:com.phqb.crawler.Utils.extractStorageAmount($productTitle)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </storageAmountInGB>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:variable name="rawScreenInfo">
                <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'man-hinh', $quot)), '},')"/>
            </xsl:variable>
            <xsl:variable name="rawScreenValues">
                <xsl:value-of select="substring-before(substring-after($rawScreenInfo, 'values'), '],')"/>
            </xsl:variable>
            <xsl:variable name="rawScreenLabel">
                <xsl:value-of select="substring-before(substring-after($rawScreenValues, concat($quot, 'label', $quot, ': ', $quot)), concat($quot, ','))"/>
            </xsl:variable>
            <xsl:variable name="rawScreenSize">
                <xsl:value-of select="normalize-space(substring-before($rawScreenLabel, 'Inch'))"/>
            </xsl:variable>
            <xsl:variable name="screenSize">
                <xsl:choose>
                    <xsl:when test="contains($rawScreenSize, 'inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, 'Inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'Inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, $quot)">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, $quot))"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="string-length($screenSize) > 0">
                    <screenSizeInInch>
                        <xsl:value-of select="translate(translate($screenSize, translate($screenSize, '0123456789,.', ''), ''), ',', '.')"/>
                    </screenSizeInInch>
                </xsl:when>
                <xsl:otherwise>
                    <screenSizeInInch xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <screenResolution>
                <type>
                    <xsl:value-of select="normalize-space(substring-after($rawScreenLabel, 'Inch'))"/>
                </type>
                <width xsi:nil="true"/>
                <height xsi:nil="true"/>
            </screenResolution>
            <screenTechnology xsi:nil="true"/>
            <touchScreen xsi:nil="true"/>
            <xsl:variable name="rawGpuInfo">
                <xsl:value-of select="substring-before(substring-after($rawProductConfig, concat($quot, 'card-do-hoa', $quot)), '},')"/>
            </xsl:variable>
            <xsl:variable name="rawGpuValues">
                <xsl:value-of select="substring-before(substring-after($rawGpuInfo, 'values'), '],')"/>
            </xsl:variable>
            <xsl:variable name="rawGpuLabel">
                <xsl:value-of select="substring-before(substring-after($rawGpuValues, concat($quot, 'label', $quot, ': ', $quot)), concat($quot, ','))"/>
            </xsl:variable>
            <graphicCard>
                <name>
                    <xsl:value-of select="normalize-space($rawGpuLabel)"/>
                </name>
                <memoryInGB xsi:nil="true"/>
                <integratedOrDedicated>
                    <xsl:choose>
                        <xsl:when test="contains($rawGpuLabel, 'Intel')">
                            <xsl:text>true</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>false</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </integratedOrDedicated>
            </graphicCard>
            <batteryInformation xsi:nil="true"/>
            <batteryLifeInHour>
                <min xsi:nil="true"/>
                <max xsi:nil="true"/>
            </batteryLifeInHour>
            <audioTechnology xsi:nil="true"/>
            <webcamResolution xsi:nil="true"/>
            <weightInKg xsi:nil="true"/>
        </product>
    </xsl:template>
</xsl:stylesheet>