<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:java="http://xml.apache.org/xalan/java"
                exclude-result-prefixes="java"
                xmlns="phqb.com/product">
    <xsl:output method="xml" indent="yes"/>
    <xsl:param name="rawStatus"/>
    <xsl:variable name="quot">"</xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="//body[@class = 'page-productdetail']" mode="productDetail"/>
    </xsl:template>

    <xsl:template match="//body[@class = 'page-productdetail']" mode="productDetail">
        <xsl:variable name="productTitle">
            <xsl:value-of select="normalize-space(//div[@class = 'productdetail-title']/h1)"/>
        </xsl:variable>
        
        <xsl:variable name="brand">
            <xsl:value-of select="substring-before($productTitle, ' ')"/>
        </xsl:variable>

        <product>
            <url/>
            <condition>
                <xsl:choose>
                    <xsl:when test="contains($productTitle, 'Mới')">
                        <isNew>true</isNew>
                        <newPercent>100</newPercent>
                    </xsl:when>
                    <xsl:otherwise>
                        <isNew>false</isNew>
                        <newPercent xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
            </condition>
            <brand>
                <xsl:value-of select="$brand"/>
            </brand>
            <model>
                <xsl:value-of select="java:com.phqb.crawler.Utils.extractProductModel($productTitle)"/>
            </model>
            <xsl:variable name="price">
                <xsl:value-of select="translate(substring-before(//div[@class = 'price']/price[@class = 'last_price'], ' '), '.', '')"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$price > 0">
                    <price>
                        <xsl:value-of select="$price"/>
                    </price>
                </xsl:when>
                <xsl:otherwise>
                    <price xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="$rawStatus = 'HẾT HÀNG'">
                    <status><xsl:text>out_of_stock</xsl:text></status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'SẴN HÀNG' or $rawStatus = 'MỚI VỀ'">
                    <status><xsl:text>in_stock</xsl:text></status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'SẮP VỀ'">
                    <status><xsl:text>coming_soon</xsl:text></status>
                </xsl:when>
                <xsl:when test="$rawStatus = 'LIÊN HỆ'">
                    <status><xsl:text>contact</xsl:text></status>
                </xsl:when>
                <xsl:when test="contains($rawStatus, 'CHIẾC DUY NHẤT')">
                    <status><xsl:value-of select="substring-before($rawStatus, ' ')"/></status>
                </xsl:when>
                <xsl:otherwise>
                    <status xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <availableAt/>
            <cpuTechnology>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[1]//div[@class='giatri-ts'])[1])"/>
            </cpuTechnology>
            <cpuType>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[1]//div[@class='giatri-ts'])[2])"/>
            </cpuType>
            <cpuSpeed>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[1]//div[@class='giatri-ts'])[3])"/>
            </cpuSpeed>
            <cpuCache>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[1]//div[@class='giatri-ts'])[4])"/>
            </cpuCache>
            <xsl:variable name="rawRAM">
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[2]//div[@class='giatri-ts'])[1])"/>
            </xsl:variable>
            <ramInGB>
                <xsl:value-of select="normalize-space(substring-before(translate($rawRAM, 'GB', 'gb'), 'gb'))"/>
            </ramInGB>
            <ramType>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[2]//div[@class='giatri-ts'])[2])"/>
            </ramType>
            <ramBus>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[2]//div[@class='giatri-ts'])[3])"/>
            </ramBus>
            <xsl:variable name="rawStorage">
                <xsl:value-of select="((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[2]//div[@class='giatri-ts'])[4]"/>
            </xsl:variable>
            <xsl:variable name="firstStorage">
                <xsl:choose>
                    <xsl:when test="string-length(normalize-space(substring-before($rawStorage, '+'))) > 0">
                        <xsl:value-of select="normalize-space(substring-before($rawStorage, '+'))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$rawStorage"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <storageType>
                <xsl:if test="contains($firstStorage, 'SSD')">
                    <xsl:text>SSD</xsl:text>
                </xsl:if>
                <xsl:if test="contains($firstStorage, 'HDD')">
                    <xsl:text>HDD</xsl:text>
                </xsl:if>
            </storageType>
            <xsl:variable name="rawStorageAmount">
                <xsl:choose>
                    <xsl:when test="contains(translate($firstStorage, 'TGB', 'tgb'), 'gb')">
                        <xsl:value-of select="normalize-space(substring-before(translate($firstStorage, 'TGB', 'tgb'), 'gb'))"/>
                    </xsl:when>
                    <xsl:when test="contains(translate($firstStorage, 'TGB', 'tgb'), 'tb')">
                        <xsl:value-of select="normalize-space(substring-before(translate($firstStorage, 'TGB', 'tgb'), 'tb'))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(substring-before($firstStorage, ' '))"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="storageAmount">
                <xsl:value-of select="translate(translate($rawStorageAmount, translate($rawStorageAmount, '0123456789,.', ''), ''), ',', '.')"/>
            </xsl:variable>
            <storageAmountInGB>
                <xsl:choose>
                    <xsl:when test="contains(translate($firstStorage, 'TGB', 'tgb'), 'gb')">
                        <xsl:value-of select="$storageAmount"/>
                    </xsl:when>
                    <xsl:when test="contains(translate($firstStorage, 'TGB', 'tgb'), 'tb')">
                        <xsl:value-of select="$storageAmount * 1000"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$storageAmount"/>
                    </xsl:otherwise>
                </xsl:choose>
            </storageAmountInGB>
            <xsl:variable name="rawScreenSize">
                <xsl:value-of select="((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[3]//div[@class='giatri-ts'])[1]"/>
            </xsl:variable>
            <xsl:variable name="screenSize">
                <xsl:choose>
                    <xsl:when test="contains($rawScreenSize, 'inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, 'Inch')">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, 'Inch'))"/>
                    </xsl:when>
                    <xsl:when test="contains($rawScreenSize, $quot)">
                        <xsl:value-of select="normalize-space(substring-before($rawScreenSize, $quot))"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="string-length($screenSize) > 0">
                    <screenSizeInInch>
                        <xsl:value-of select="translate(translate($screenSize, translate($screenSize, '0123456789,.', ''), ''), ',', '.')"/>
                    </screenSizeInInch>
                </xsl:when>
                <xsl:otherwise>
                    <screenSizeInInch xsi:nil="true"/>
                </xsl:otherwise>
            </xsl:choose>
            <screenResolution>
                <xsl:variable name="rawScreenDimension">
                    <xsl:value-of select="translate(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[3]//div[@class='giatri-ts'])[2], 'X*', 'xx')"/>
                </xsl:variable>
                <xsl:variable name="rawWidth">
                    <xsl:value-of select="substring-before($rawScreenDimension, 'x')"/>
                </xsl:variable>
                <xsl:variable name="rawHeight">
                    <xsl:value-of select="substring-after($rawScreenDimension, 'x')"/>
                </xsl:variable>
                
                <type>
                    <xsl:value-of select="normalize-space(translate($rawScreenDimension, '0123456789()xKk', ''))"/>
                </type>
                <width>
                    <xsl:value-of select="translate($rawWidth, translate($rawWidth, '0123456789', ''), '')"/>
                </width>
                <height>
                    <xsl:value-of select="translate($rawHeight, translate($rawHeight, '0123456789', ''), '')"/>
                </height>
            </screenResolution>
            <screenTechnology>
                <xsl:value-of select="((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[3]//div[@class='giatri-ts'])[3]"/>
            </screenTechnology>
            <touchScreen>
                <xsl:variable name="rawTouchScreen">
                    <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[3]//div[@class='giatri-ts'])[4])"/>
                </xsl:variable>
                <xsl:if test="$rawTouchScreen = 'Không'">
                    <xsl:text>false</xsl:text>
                </xsl:if>
                <xsl:if test="not($rawTouchScreen = 'Không')">
                    <xsl:text>true</xsl:text>
                </xsl:if>
            </touchScreen>
            <graphicCard>
                <name>
                    <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[4]//div[@class='giatri-ts'])[1])"/>
                </name>
                <xsl:variable name="rawMemory">
                    <xsl:value-of select="((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[4]//div[@class='giatri-ts'])[2]"/>
                </xsl:variable>
                <xsl:variable name="memory">
                    <xsl:value-of select="normalize-space(substring-before(translate($rawMemory, 'GB', 'gb'), 'gb'))"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($memory) > 0">
                        <memoryInGB>
                            <xsl:value-of select="translate($memory, ',', '.')"/>
                        </memoryInGB>
                    </xsl:when>
                    <xsl:otherwise>
                        <memoryInGB xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                <integratedOrDedicated>
                    <xsl:variable name="rawIntegratedOrDedicated">
                        <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[4]//div[@class='giatri-ts'])[3])"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$rawIntegratedOrDedicated = 'Rời'">
                            <xsl:text>false</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>true</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </integratedOrDedicated>
            </graphicCard>
            <batteryInformation>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[5]//div[@class='giatri-ts'])[1])"/>
            </batteryInformation>
            <batteryLifeInHour>
                <xsl:variable name="rawBatteryLife">
                    <xsl:value-of select="((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[5]//div[@class='giatri-ts'])[2]"/>
                </xsl:variable>
                <xsl:variable name="rawMin">
                    <xsl:value-of select="normalize-space(substring-before($rawBatteryLife, '-'))"/>
                </xsl:variable>
                <xsl:variable name="rawMax">
                    <xsl:value-of select="normalize-space(substring-before(substring-after($rawBatteryLife, '-'), 'h'))"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="string-length($rawMin) > 0">
                        <min>
                            <xsl:value-of select="$rawMin"/>
                        </min>
                    </xsl:when>
                    <xsl:otherwise>
                        <min xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="string-length($rawMax) > 0">
                        <max>
                            <xsl:value-of select="$rawMax"/>
                        </max>
                    </xsl:when>
                    <xsl:otherwise>
                        <max xsi:nil="true"/>
                    </xsl:otherwise>
                </xsl:choose>
            </batteryLifeInHour>
            <audioTechnology>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[4]//div[@class='giatri-ts'])[4])"/>
            </audioTechnology>
            <webcamResolution>
                <xsl:value-of select="normalize-space(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[8]//div[@class='giatri-ts'])[2])"/>
            </webcamResolution>
            <xsl:variable name="rawWeight">
                <xsl:value-of select="normalize-space(substring-before(translate(((//div[@id = 'popup-thongsochitiet']//li[@class = 'ts_1'])[7]//div[@class='giatri-ts'])[2], 'KG', 'kg'), 'kg'))"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="contains(translate($rawWeight, 'KG', 'kg'), 'kg')">
                    <weightInKg>
                        <xsl:value-of select="normalize-space(translate(substring-before(translate($rawWeight, 'KG', 'kg'), 'kg'), ',', '.'))"/>
                    </weightInKg>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="weight">
                        <xsl:value-of select="translate(translate($rawWeight, translate($rawWeight, '0123456789.,', ''), ''), ',', '.')"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="string-length($weight) > 0">
                            <weightInKg>
                                <xsl:value-of select="$weight"/>
                            </weightInKg>
                        </xsl:when>
                        <xsl:otherwise>
                            <weightInKg xsi:nil="true"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </product>
    </xsl:template>
</xsl:stylesheet>