<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="phqb.com/cpuBenchmarks">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="/TABLE/TBODY"/>
    </xsl:template>

    <xsl:template match="/TABLE/TBODY">
        <cpus>
            <xsl:apply-templates select="TR"/>
        </cpus>
    </xsl:template>

    <xsl:template match="TR">
        <cpu>
            <name><xsl:value-of select="normalize-space(TD[1])"/></name>
            <passmarkMark><xsl:value-of select="TD[2]"/></passmarkMark>
        </cpu>
    </xsl:template>
</xsl:stylesheet>