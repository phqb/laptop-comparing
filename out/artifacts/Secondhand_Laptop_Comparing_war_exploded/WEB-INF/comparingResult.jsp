<%@ page import="com.phqb.web.ComparingServlet" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<% request.setCharacterEncoding("utf-8"); %>
<!doctype html>
<html lang="en">
<jsp:include page="/WEB-INF/head.jsp">
    <jsp:param name="title" value="Laptop Comparing - So sánh Laptop"/>
</jsp:include>
<body>
<jsp:include page="/WEB-INF/header.jsp"/>
<h2>Tiêu chí so sánh</h2>
<ol>
    <c:forEach items="${requestScope.comparingFields}" var="field">
        <li style="padding-top: 4px; padding-bottom: 4px">
            <%= com.phqb.utils.Utils.localizeProductCompareCriteria((String) pageContext.getAttribute("field")) %>
            <%= ComparingServlet.LOWER_IS_BETTER.contains(pageContext.getAttribute("field")) ? "(Càng thấp càng tốt)" : "" %>
        </li>
    </c:forEach>
</ol>
<div style="margin-top: 32px">
    <c:choose>
        <c:when test="${not empty requestScope.comparingProducts}">
            <c:set var="products" value="${requestScope.comparingProducts}" scope="request"/>
            <c:set var="isComparing" value="${true}" scope="request"/>
            <jsp:include page="/WEB-INF/products.jsp"/>
        </c:when>
        <c:otherwise>
            <div style="display: flex; flex-direction: column; justify-content: center">
                <p>Không có sản phẩm nào để so sánh.</p>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
