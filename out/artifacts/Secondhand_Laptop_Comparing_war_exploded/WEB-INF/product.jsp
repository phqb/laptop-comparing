<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="pure-g">
    <div class="pure-u-md-1-6"
         style="display: flex; flex-direction: column; justify-content: center; align-items: center">
        <img src="https://via.placeholder.com/150"/>
    </div>
    <div class="pure-u-md-2-3">
        <div style="display: flex; flex-direction: row; align-items: center">
            <c:choose>
                <c:when test="${requestScope.product.conditionIsNew}">
                    <button class="pure-button button-success button-xsmall"
                            style="margin-right: 16px">
                        Mới
                    </button>
                </c:when>
                <c:otherwise>
                    <button class="pure-button button-warning button-xsmall"
                            style="margin-right: 16px">
                        <c:choose>
                            <c:when test="${not empty requestScope.product.conditionNewPercent}">
                                Cũ (<f:formatNumber maxFractionDigits="0"
                                                    value="${requestScope.product.conditionNewPercent}"/>%)
                            </c:when>
                            <c:otherwise>
                                Cũ
                            </c:otherwise>
                        </c:choose>
                    </button>
                </c:otherwise>
            </c:choose>
            <h2>
                ${requestScope.product.brand} ${requestScope.product.model}
            </h2>
            <button id="btnAddComparing${requestScope.product.id}" class="pure-button button-secondary"
                    style="margin-left: 16px;"
                    onclick="toggleComparing(${requestScope.product.id})">+ So sánh
            </button>
            <script>
                (function () {
                    if (localStorage.getItem('comparingIds')) {
                        let comparingIds = JSON.parse(localStorage.getItem('comparingIds'));

                        if (comparingIds.indexOf(${requestScope.product.id}) >= 0) {
                            document.getElementById('btnAddComparing${requestScope.product.id}').classList.remove('button-secondary');
                            document.getElementById('btnAddComparing${requestScope.product.id}').classList.add('button-error');
                            document.getElementById('btnAddComparing${requestScope.product.id}').textContent = '- So sánh';
                        }
                    }
                })();
            </script>
        </div>
        <div class="pure-g">
            <div class="pure-u-md-1-2">
                <p>
                    Tình trạng:
                    <c:choose>
                        <c:when test="${requestScope.product.status == 'in_stock'}">
                            Còn hàng
                        </c:when>
                        <c:when test="${requestScope.product.status == 'out_of_stock'}">
                            Hết hàng
                        </c:when>
                        <c:when test="${requestScope.product.status == 'comming_soon'}">
                            Sắp về
                        </c:when>
                        <c:when test="${requestScope.product.status == 'contact'}">
                            Liên hệ
                        </c:when>
                        <c:otherwise>
                            Không xác định
                        </c:otherwise>
                    </c:choose>
                </p>
                <p>
                    Có sẵn tại:
                    <c:choose>
                        <c:when test="${not empty requestScope.product.joinedAvailableAt}">
                            ${requestScope.product.joinedAvailableAt}
                        </c:when>
                        <c:otherwise>
                            Không có thông tin
                        </c:otherwise>
                    </c:choose>
                </p>
            </div>
            <div class="pure-u-md-1-2" style="display: flex; flex-direction: column">
                <h4>Thông số chi tiết</h4>
                <table class="pure-table pure-table-bordered" style="width: 100%">
                    <tbody>
                    <tr>
                        <td>CPU</td>
                        <td>
                            ${requestScope.product.cpuType} ${requestScope.product.cpuTechnology} ${requestScope.product.cpuCache}
                            <c:if test="${not empty requestScope.product.cpuSpeed}">
                                @ ${requestScope.product.cpuSpeed}
                            </c:if>
                        </td>
                    </tr>
                    <tr>
                        <td>Điểm CPU</td>
                        <td>
                            <c:if test="${not empty requestScope.product.cpuPassmarkMark}">
                                ${requestScope.product.cpuPassmarkMark}
                            </c:if>
                            <c:if test="${empty requestScope.product.cpuPassmarkMark}">
                                Không xác định
                            </c:if>
                        </td>
                    </tr>
                    <tr>
                        <td>RAM</td>
                        <td>
                            ${requestScope.product.ramInGB}
                            Gb ${requestScope.product.ramType} ${requestScope.product.ramBus}
                        </td>
                    </tr>
                    <tr>
                        <td>Ổ cứng</td>
                        <td>
                            ${requestScope.product.storageType} ${requestScope.product.storageAmountInGB} Gb
                        </td>
                    </tr>
                    <tr>
                        <td>Card đồ họa</td>
                        <td>
                            <c:choose>
                                <c:when test="${requestScope.product.graphicCardIntegratedOrDedicated == true}">
                                    Tích hợp
                                </c:when>
                                <c:when test="${requestScope.product.graphicCardIntegratedOrDedicated == false}">
                                    Rời
                                </c:when>
                            </c:choose>
                            ${requestScope.product.graphicCardName} ${requestScope.product.graphicCardMemoryInGB}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div id="detail${requestScope.product.id}"
                     style=" height: 0; overflow: hidden; transition: height 0.25s">
                    <table class="pure-table pure-table-bordered" style="width: 100%; border-top: 0">
                        <tbody>
                        <tr>
                            <td>Màn hình</td>
                            <td>
                                ${requestScope.product.screenSizeInInch} Inch
                                ${requestScope.product.screenResolutionType}
                                <c:if test="${requestScope.product.screenResolutionWidth != null}">
                                    ${requestScope.product.screenResolutionWidth} x ${requestScope.product.screenResolutionHeight}
                                </c:if>
                                ${requestScope.product.screenTechnology}
                                <c:if test="${requestScope.product.touchScreen == true}">
                                    Cảm ứng
                                </c:if>
                            </td>
                        </tr>
                        <tr>
                            <td>Thời lượng pin</td>
                            <td>
                                ${requestScope.product.batteryInformation}
                                ${requestScope.product.batteryLifeInHourMin}
                                - ${requestScope.product.batteryLifeInHourMax}
                                giờ
                            </td>
                        </tr>
                        <tr>
                            <td>Công nghệ âm thanh</td>
                            <td>${requestScope.product.audioTechnology}</td>
                        </tr>
                        <tr>
                            <td>Webcam</td>
                            <td>${requestScope.product.webcamResolution}</td>
                        </tr>
                        <tr>
                            <td>Trọng lượng</td>
                            <td>${requestScope.product.weightInKg} Kg</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a href="javascript:void(0)"
                   style="align-self: center; text-decoration: underline; margin-top: 4px"
                   onclick="if (this.textContent === 'Xem thêm') {
                           document.getElementById('detail${requestScope.product.id}').style.height = 'auto';
                           this.textContent = 'Thu gọn';
                           } else {
                           document.getElementById('detail${requestScope.product.id}').style.height = '0';
                           this.textContent = 'Xem thêm';
                           }">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="pure-u-md-1-6"
         style="display: flex; flex-direction: column; justify-content: center; align-items: center">
        <h4 style="color: red">
            <f:formatNumber currencyCode="VND" value="${requestScope.product.price}"/> đ
        </h4>
        <a class="pure-button pure-button-primary" href="${requestScope.product.url}" target="_blank">
            Tới nơi bán
        </a>
    </div>
</div>
