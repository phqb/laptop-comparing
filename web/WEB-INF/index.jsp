<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<% request.setCharacterEncoding("utf-8"); %>
<!doctype html>
<html lang="en">
<jsp:include page="/WEB-INF/head.jsp">
    <jsp:param name="title" value="Laptop Comparing - So sánh Laptop"/>
</jsp:include>
<body>
<jsp:include page="/WEB-INF/header.jsp"/>
<div style="margin-top: 32px">
    <c:choose>
        <c:when test="${not empty requestScope.searchResult}">
            <c:set var="products" value="${requestScope.searchResult}" scope="request"/>
            <c:set var="isComparing" value="${false}" scope="request"/>
            <jsp:include page="/WEB-INF/products.jsp"/>
            <div class="pagination">
                <script>
                    function gotoPage(pageNum) {
                        if (pageNum) {
                            location.replace('${pageContext.servletContext.contextPath}/search'
                                + '?keyword=${requestScope.currentKeyword}'
                                + '&offset=' + ((pageNum - 1) * ${requestScope.currentLimit})
                                + '&limit=${requestScope.currentLimit}');
                        }
                    }
                </script>
                <c:set var="lastPage" value="0"/>
                <c:forEach items="${requestScope.pageNumberList}" var="pageNum">
                    <c:if test="${pageNum - lastPage > 1}">
                        <div class="pagination-item">
                            <a href="javascript:void(0)" style="font-weight: bold"
                               onclick="gotoPage(window.parseInt(window.prompt('Nhập trang cần đến:')))">
                                ...
                            </a>
                        </div>
                    </c:if>
                    <c:choose>
                        <c:when test="${pageNum == requestScope.currentPage}">
                            <div class="pagination-item" style="font-weight: bold">${pageNum}</div>
                        </c:when>
                        <c:otherwise>
                            <div class="pagination-item">
                                <a href="${pageContext.servletContext.contextPath}/search?keyword=${requestScope.currentKeyword}&offset=${(pageNum - 1) * requestScope.currentLimit}&limit=${requestScope.currentLimit}">
                                        ${pageNum}
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <c:set var="lastPage" value="${pageNum}"/>
                </c:forEach>
            </div>
        </c:when>
        <c:otherwise>
            <div style="display: flex; flex-direction: column; justify-content: center">
                <p>Hiện không có sản phẩm nào. Vui lòng quay lại lần sau!</p>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
