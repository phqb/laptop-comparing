<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>

<div class="pure-g" style="margin-top: 16px">
    <a class="pure-u-md-1-5" href="${pageContext.servletContext.contextPath}">
        <h1>So sánh Laptop</h1>
    </a>
    <div class="pure-u-md-4-5"
         style="display: flex; flex-direction: row; justify-content: space-between; align-items: center">
        <div class="autocomplete" style="flex-grow: 1;">
            <form action="search" method="get" autocomplete="off">
                <input name="keyword" id="search-input" type="text" autocomplete="new-keyword"
                       placeholder="Tìm kiếm sản phẩm" style="width: 100%; padding: 8px"/>
                <input type="submit" style="display: none"/>
            </form>
        </div>
        <script src="js/autocomplete.js"></script>
        <script>
            let keywords = [];

            (function () {
                let request = new XMLHttpRequest();
                request.open('GET', '${pageContext.servletContext.contextPath}/api/search_keywords', true);

                request.onload = function () {
                    if (this.status >= 200 && this.status < 400) {
                        keywords = JSON.parse(this.responseText);
                        autocomplete(document.getElementById('search-input'), keywords);
                    }
                };

                request.send();
            })();
        </script>
        <script>
            function toComparingPage() {
                let comparingIds = JSON.parse(localStorage.getItem('comparingIds')) || [];
                let comparingFields = ["ConditionNewPercent", "Price", "CpuPassmarkMark", "RamInGB", "StorageAmountInGB", "ScreenSizeInInch", "ScreenResolutionWidth", "GraphicCardMemoryInGB", "GraphicCardIntegratedOrDedicated"];
                window.open('${pageContext.servletContext.contextPath}/compare'
                    + '?comparingIds='
                    + comparingIds.join('&comparingIds=')
                    + '&comparingFields='
                    + comparingFields.join('&comparingFields='));
            }
        </script>
        <div id="btnCompare" class="pure-button pure-button-primary" style="flex-grow: 0; margin-left: 16px;"
             onclick="toComparingPage()">
            So sánh
        </div>
        <script>
            (function () {
                if (localStorage.getItem("comparingIds")) {
                    let comparingIds = JSON.parse(localStorage.getItem("comparingIds"));
                    let numOfProducts = comparingIds.length;
                    if (numOfProducts > 1) {
                        document.getElementById('btnCompare').textContent = 'So sánh ' + numOfProducts + ' sản phẩm'
                    }
                }
            })();
        </script>
    </div>
</div>
<script>
    function toggleComparing(productId) {
        if (localStorage.getItem('comparingIds') === undefined) {
            localStorage.setItem('comparingIds', JSON.stringify([]));
        }

        let comparingIds = JSON.parse(localStorage.getItem('comparingIds'));

        if (document.getElementById('btnAddComparing' + productId).textContent === '+ So sánh') {
            if (comparingIds.indexOf(productId) < 0) {
                comparingIds.push(productId);

                if (comparingIds.length > 1) {
                    document.getElementById('btnCompare').textContent = 'So sánh ' + comparingIds.length + ' sản phẩm';
                }

                document.getElementById('btnAddComparing' + productId).classList.remove('button-secondary');
                document.getElementById('btnAddComparing' + productId).classList.add('button-error');
                document.getElementById('btnAddComparing' + productId).textContent = '- So sánh';
            }
        } else {
            if (comparingIds.indexOf(productId) >= 0) {
                comparingIds.splice(comparingIds.indexOf(productId), 1);

                if (comparingIds.length > 1) {
                    document.getElementById('btnCompare').textContent = 'So sánh ' + comparingIds.length + ' sản phẩm';
                } else {
                    document.getElementById('btnCompare').textContent = 'So sánh';
                }

                document.getElementById('btnAddComparing' + productId).classList.remove('button-error');
                document.getElementById('btnAddComparing' + productId).classList.add('button-secondary');
                document.getElementById('btnAddComparing' + productId).textContent = '+ So sánh';
            }
        }

        localStorage.setItem('comparingIds', JSON.stringify(comparingIds));
    }
</script>
