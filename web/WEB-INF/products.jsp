<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${requestScope.products}" var="product" varStatus="i">
    <div id="product${product.id}" class="pure-g card" style="padding-top: 16px; padding-bottom: 16px">
        <c:set var="product" value="${product}" scope="request"/>
        <c:if test="${requestScope.isComparing}">
            <div class="pure-u-md-1-12"
                 style="display: flex; flex-direction: column; justify-content: center; align-items: center">
                <h1 style="color: red">
                    <f:formatNumber value="${requestScope.productMarks[i.index].mark}" maxFractionDigits="1" minFractionDigits="1"/>
                </h1>
                <h2 style="color: red">
                    Điểm
                </h2>
            </div>
            <div class="pure-u-md-11-12">
                <jsp:include page="/WEB-INF/product.jsp"/>
            </div>
        </c:if>
        <c:if test="${not requestScope.isComparing}">
            <div class="pure-u-md-1-1">
                <jsp:include page="/WEB-INF/product.jsp"/>
            </div>
        </c:if>
    </div>
</c:forEach>
