<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Hepta+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css"/>
    <link rel="stylesheet" href="css/autocomplete.css"/>
    <link rel="stylesheet" href="css/pure-min.css"/>
    <link rel="stylesheet" href="css/grids-responsive-min.css"/>
    <link rel="stylesheet" href="css/button.css"/>
    <title>${param.title}</title>
</head>
